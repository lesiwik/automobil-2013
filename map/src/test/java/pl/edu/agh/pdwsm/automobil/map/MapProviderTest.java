package pl.edu.agh.pdwsm.automobil.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertices;

import com.google.common.collect.Iterables;

import edu.uci.ics.jung.graph.Graph;

public class MapProviderTest {

	private final Graph<Vertex, Edge> graph;
	private final Vertex startPoint;
	private final Vertex endPoint;
	private final MapProviderService mapProvider;

	public MapProviderTest() throws Exception {
		mapProvider = new MapProviderService();

		startPoint = Vertices.newVertex(50.049568, 19.932117);
		endPoint = Vertices.newVertex(50.053419, 19.928985);
		graph = mapProvider.getGraph(startPoint, endPoint);
	}

	@Test
	@Ignore
	public void bigGraph() throws Exception {
		Vertex startPoint = Vertices.newVertex(50.0741, 19.9113);
		Vertex endPoint = Vertices.newVertex(50.0403, 19.9808);
		Graph<Vertex, Edge> bigGraph = mapProvider.getGraph(startPoint, endPoint);
		
		assertTrue("Wrong vertex count", bigGraph.getVertexCount() > 5000);
		assertTrue("Wrong edge count", bigGraph.getEdgeCount() > 10000);
	}

	@Test
	@Ignore
	public void saveForSimpulation() throws Exception {
		Vertex startPoint = Vertices.newVertex(50.094, 19.8849);
		Vertex endPoint = Vertices.newVertex(50.0383, 20.0113);
		Graph<Vertex, Edge> graph = mapProvider.getGraph(startPoint, endPoint);
		Collection<Vertex> vertices = new ArrayList<Vertex>(graph.getVertices());
		
		ObjectOutputStream verticesOut = new ObjectOutputStream(new FileOutputStream("cracow.vertices"));
		verticesOut.writeObject(vertices);
		verticesOut.close();
	}
	
	@Test
	public void verticesCount() throws Exception {
		assertTrue("Wrong vertex count", graph.getVertexCount() > 500);
	}

	@Test
	public void edgesCount() throws Exception {
		assertTrue("Wrong edge count", graph.getEdgeCount() > 1000);
	}

	@Test
	public void isFirstPointNear() throws Exception {
		Vertex someVertex = Iterables.getFirst(graph.getVertices(), null);
		assertEquals(startPoint.getLatitude(), someVertex.getLatitude(), 0.5);
		assertEquals(startPoint.getLongitude(), someVertex.getLongitude(), 0.5);
	}

	@Test
	public void approximation() throws Exception {
		double two = Vertices.approximation(2).reducePrecision(1.83913);
		assertEquals(1.83, two, 0.0);

		double three = Vertices.approximation(3).reducePrecision(1.83913);
		assertEquals(1.839, three, 0.0);

		double five = Vertices.approximation(5).reducePrecision(1.83913);
		assertEquals(1.83913, five, 0.0);

		double ten = Vertices.approximation(10).reducePrecision(1.83913);
		assertEquals(1.83913, ten, 0.0);
		
		Vertex approx = Vertices.approximation(2).newVertex(50.049568, 19.932117);
		assertEquals(Vertices.newVertex(50.04, 19.93), approx);
	}
}
