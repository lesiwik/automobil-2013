package pl.edu.agh.pdwsm.automobil.map;

import org.geotools.data.DataStore;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.MapGraph;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertices;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertices.ApproxVertexBuilder;
import pl.edu.agh.pdwsm.automobil.ontology.DistanceCalculator;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;

public class MapProviderService implements MapProvider {

    private final DataStore dataStore;

    public MapProviderService() throws Exception {
        dataStore = MapDatabase.getDatabase();
    }

    @Override
    public Graph<Vertex, Edge> getGraph(Vertex startPoint, Vertex endPoint) throws Exception {
        return new GraphProvider(startPoint, endPoint).getGraph();
    }

    private class GraphProvider {

        private final static int DIGITS_AFTER_DOT = 4;

        private final static double EXTRA_MAP_DEGREE = 0.01;

        private final Vertex startPoint;

        private final Vertex endPoint;

        private final Nearest nearestToStart;

        private final Nearest nearestToEnd;

        private final ApproxVertexBuilder precisionReducer;

        public GraphProvider(Vertex startPoint, Vertex endPoint) {
            this.startPoint = startPoint;
            this.endPoint = endPoint;
            this.nearestToStart = new Nearest(startPoint);
            this.nearestToEnd = new Nearest(endPoint);
            this.precisionReducer = Vertices.approximation(DIGITS_AFTER_DOT);
        }

        public Graph<Vertex, Edge> getGraph() throws Exception {
            Graph<Vertex, Edge> graph = new MapGraph(startPoint, endPoint);

            double minLongitude, maxLongitude, minLatitude, maxLatitude;
            minLongitude = Math.min(startPoint.getLongitude(), endPoint.getLongitude()) - EXTRA_MAP_DEGREE;
            maxLongitude = Math.max(startPoint.getLongitude(), endPoint.getLongitude()) + EXTRA_MAP_DEGREE;
            minLatitude = Math.min(startPoint.getLatitude(), endPoint.getLatitude()) - EXTRA_MAP_DEGREE;
            maxLatitude = Math.max(startPoint.getLatitude(), endPoint.getLatitude()) + EXTRA_MAP_DEGREE;
            Vertex bottomLeft = Vertices.newVertex(minLatitude, minLongitude);
            Vertex topRight = Vertices.newVertex(maxLatitude, maxLongitude);
            SimpleFeatureIterator iter = getMapRawElements(bottomLeft, topRight).features();
            while (iter.hasNext()) {
                SimpleFeature feature = iter.next();

                Street street = Street.newFromFeature(feature);

                boolean oneway = false;
                if (street.isOneWay()) {
                    oneway = true;
                }

                for (int i = 0; i < street.getGeometry().size() - 1; i++) {
                    Vertex exactStart = street.getGeometry().get(i);
                    Vertex approxStart = precisionReducer.newVertex(exactStart);
                    graph.addVertex(approxStart);

                    Vertex exactEnd = street.getGeometry().get(i + 1);
                    Vertex approxEnd = precisionReducer.newVertex(exactEnd);
                    graph.addVertex(approxEnd);

                    nearestToStart.updateIfNearest(approxStart);
                    nearestToStart.updateIfNearest(approxEnd);

                    nearestToEnd.updateIfNearest(approxStart);
                    nearestToEnd.updateIfNearest(approxEnd);

                    double distance = DistanceCalculator.calculateDistance(approxStart, approxEnd);
                    Edge edge = new Edge(approxStart, approxEnd, distance, street.getMaxSpeed());
                    graph.addEdge(edge, approxStart, approxEnd, EdgeType.DIRECTED);
                    if (!oneway) {
                        Edge edgeOpposite = new Edge(approxEnd, approxStart, distance, street.getMaxSpeed());
                        graph.addEdge(edgeOpposite, approxEnd, approxStart, EdgeType.DIRECTED);
                    }
                }
            }

            makeNearestEdge(startPoint, nearestToStart, graph);
            makeNearestEdge(endPoint, nearestToEnd, graph);

            return graph;
        }

        private void makeNearestEdge(Vertex vertex, Nearest neares, Graph<Vertex, Edge> graph) {
            if (vertex.equals(neares.getNearest())) {
                return;
            }
            graph.addVertex(neares.getNearest());
            Edge edge = new Edge(vertex, neares.getNearest(), neares.getNearestDistance(), 50);
            graph.addEdge(edge, vertex, neares.getNearest(), EdgeType.DIRECTED);
            edge = new Edge(neares.getNearest(), vertex, neares.getNearestDistance(), 50);
            graph.addEdge(edge, neares.getNearest(), vertex, EdgeType.DIRECTED);
        }

        private SimpleFeatureCollection getMapRawElements(Vertex startPoint, Vertex endPoint) throws Exception {
            String typeName = dataStore.getTypeNames()[0];
            SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeName);

            Filter box = Filters.newBoxFilter(startPoint, endPoint);
            Filter names = Filters.newStreetsWithNamesFilter();
            Filter types = Filters.newAcceptedTypesFilter();

            Filter allFilters = Filters.and(box, types);
            return featureSource.getFeatures(allFilters);
        }
    }

    private static class Nearest {

        private final Vertex from;

        private Vertex nearest;

        private double nearestDistance;

        public Nearest(Vertex from) {
            this.from = from;
        }

        public void updateIfNearest(Vertex candidate) {
            double candidateDistance = DistanceCalculator.calculateDistance(from, candidate);
            if (nearest == null || candidateDistance < nearestDistance) {
                nearest = candidate;
                nearestDistance = candidateDistance;
            }
        }

        public Vertex getNearest() {
            return nearest;
        }

        public double getNearestDistance() {
            return nearestDistance;
        }
    }
}
