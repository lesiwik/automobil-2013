package pl.edu.agh.pdwsm.automobil.map;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertices;
import edu.uci.ics.jung.graph.Graph;

public class CracowMapProviderService implements MapProvider {

	private Graph<Vertex, Edge> graph;

	public CracowMapProviderService() {
		try {
			Vertex startPoint = Vertices.newVertex(50.094, 19.8849);
			Vertex endPoint = Vertices.newVertex(50.0383, 20.0113);
			graph = new MapProviderService().getGraph(startPoint, endPoint);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Graph<Vertex, Edge> getGraph(Vertex startPoint, Vertex endPoint) throws Exception {
		return graph;
	}
}
