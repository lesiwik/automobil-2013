package pl.edu.agh.pdwsm.automobil.map;

import java.util.Arrays;
import java.util.List;

import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;

import com.google.common.collect.Lists;

public final class Filters {

    private static FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2();

    private Filters() {

    }

    public static Filter newBoxFilter(Vertex startPoint, Vertex endPoint) {
        ReferencedEnvelope bbox = new ReferencedEnvelope(startPoint.getLongitude(), endPoint.getLongitude(), startPoint.getLatitude(),
                endPoint.getLatitude(), DefaultGeographicCRS.WGS84);
        Filter filter = filterFactory.bbox(filterFactory.property("the_geom"), bbox);

        return filter;
    }

    public static Filter newStreetsWithNamesFilter() {
        Filter filter = filterFactory.notEqual(filterFactory.property("name"), filterFactory.literal(""));

        return filter;
    }

    public static Filter newAcceptedTypesFilter() {
        List<String> types = Lists.newArrayList( //
                "motorway", //
                "motorway_link",//
                "trunk",//
                "trunk_link",//
                "primary",//
                "primary_link",//
                "secondary",//
                "secondary_link",//
                "tertiary",//
                "tertiary_link",//
                "living_street",//
                "residential",//
                "nclassified",//
                "service",//
                "road");

        return filterFactory.or(typeToFiltres(types));
    }

    public static Filter and(Filter... filters) {
        return filterFactory.and(Arrays.asList(filters));
    }

    private static List<Filter> typeToFiltres(List<String> types) {
        List<Filter> filters = Lists.newLinkedList();

        for (String type : types) {
            filters.add(typeFiltre(type));
        }

        return filters;
    }

    private static Filter typeFiltre(String type) {
        return filterFactory.equals(filterFactory.property("type"), filterFactory.literal(type));
    }
}
