package pl.edu.agh.pdwsm.automobil.map.agent;

import jade.content.ContentManager;
import jade.content.lang.sl.SLCodec;
import jade.core.Agent;
import pl.edu.agh.pdwsm.automobil.map.MapProvider;
import pl.edu.agh.pdwsm.automobil.map.MapProviderService;
import pl.edu.agh.pdwsm.automobil.map.agent.behaviours.MapProviderResponder;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.ontology.AutomobilOntology;
import edu.uci.ics.jung.graph.Graph;

public class MapProviderAgent extends Agent implements MapProvider {

	private final MapProvider mapProviderService;

	public MapProviderAgent() throws Exception {
		registerO2AInterface(MapProvider.class, this);
		mapProviderService = new MapProviderService();
	}

	@Override
	protected void setup() {
		ContentManager cm = getContentManager();
		cm.registerLanguage(new SLCodec());
		cm.registerOntology(AutomobilOntology.getInstance());
		cm.setValidationMode(false);

		addBehaviour(new MapProviderResponder(this, mapProviderService));
	}

	@Override
	public Graph<Vertex, Edge> getGraph(Vertex startPoint, Vertex endPoint) throws Exception {
		return mapProviderService.getGraph(startPoint, endPoint);
	}
}
