package pl.edu.agh.pdwsm.automobil.map;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import edu.uci.ics.jung.graph.Graph;

public interface MapProvider {

	Graph<Vertex, Edge> getGraph(Vertex startPoint, Vertex endPoint) throws Exception;
}