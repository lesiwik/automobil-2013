package pl.edu.agh.pdwsm.automobil.map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.MultiLineString;
import org.opengis.feature.simple.SimpleFeature;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertices;

import java.util.List;
import java.util.Map;

public class Street {

    private final boolean oneWay;

    private final int maxSpeed;

    private final List<Vertex> geometry;

    public Street(Builder builder) {
        this.oneWay = builder.oneWay;
        this.maxSpeed = builder.maxSpeed;
        this.geometry = builder.geometry;
    }

    public static Street newFromFeature(SimpleFeature feature) {
        Builder builder = new Builder(feature);
        return builder.build();
    }

    public boolean isOneWay() {
        return oneWay;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public List<Vertex> getGeometry() {
        return geometry;
    }

    public static class Builder {

        private final SimpleFeature feature;

        private boolean oneWay;

        private List<Vertex> geometry;

        private int maxSpeed;

        public Builder(SimpleFeature feature) {
            this.feature = feature;
        }

        public Street build() {
            try {
                geometry = convertToVertices(feature);
                oneWay = oneWay(feature);
                maxSpeed = maxspeed(feature);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return new Street(this);
        }

        private List<Vertex> convertToVertices(SimpleFeature feature) {
            MultiLineString rawGeometry = (MultiLineString) feature.getAttribute("the_geom");
            List<Vertex> vertices = Lists.newLinkedList();

            for (Coordinate coordinate : rawGeometry.getCoordinates()) {
                Vertex vertex = Vertices.newVertex(coordinate.y, coordinate.x);
                vertices.add(vertex);
            }

            return vertices;
        }

        private boolean oneWay(SimpleFeature feature) {
            Integer oneWayValue = (Integer) feature.getAttribute("oneway");
            if (oneWayValue == null || oneWayValue == 0) {
                return false;
            }

            return true;
        }

        private int maxspeed(SimpleFeature feature) {
            String stretType = (String) feature.getAttribute("type");

            Integer maxSpeedValue = (Integer) feature.getAttribute("maxspeed");
            if (maxSpeedValue == null || maxSpeedValue == 0) {
                maxSpeedValue = getMaxspeedByStreetType(stretType);
            } else {
                maxSpeedValue = (int) (maxSpeedValue / 3.6);
            }

            return maxSpeedValue;
        }

        private int getMaxspeedByStreetType(String stretType) {
            Integer maxspeed = allowedSpped.get(stretType);
            return maxspeed != null ? maxspeed : 0;
        }

        private static final Map<String, Integer> allowedSpped = new ImmutableMap.Builder<String, Integer>(). //
                put("motorway", 40). //
                put("motorway_link", 40). //
                put("trunk", 33).//
                put("trunk_link", 33).//
                put("primary", 25).//
                put("primary_link", 25).//
                put("secondary", 25).//
                put("secondary_link", 25).//
                put("tertiary", 25).//
                put("tertiary_link", 25).//
                put("living_street", 14).//
                put("residential", 5).//
                put("nclassified", 14).//
                put("service", 5).//
                put("road", 14). // 50kmh etc
                build();

    }
}
