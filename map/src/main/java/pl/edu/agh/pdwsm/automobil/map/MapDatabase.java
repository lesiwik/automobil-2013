package pl.edu.agh.pdwsm.automobil.map;

import java.io.File;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;

public final class MapDatabase {

    private MapDatabase() {

    }

    public static DataStore getDatabase() throws Exception {
        return new MapDatabase().getOrDownloadDatabase();
    }

    public DataStore getOrDownloadDatabase() throws Exception {
        String dbLocation = "../server/db";
        if (!new File(dbLocation).exists()) {
            downloadDatabase(dbLocation);
        }

        File databaseFile = new File(dbLocation, "roads.shp");
        return getDataStoreConnection(databaseFile);
    }

    private void downloadDatabase(String dbLocation) throws Exception {
        downloadFile(dbLocation, "roads.shx");
        downloadFile(dbLocation, "roads.shp");
        downloadFile(dbLocation, "roads.dbf");
    }

    private void downloadFile(String parent, String child) throws Exception {
        Joiner urlJoiner = Joiner.on("/");
        String address = "https://dl.dropboxusercontent.com/u/16733251/mobilki";

        URL url = new URL(urlJoiner.join(address, child));
        FileUtils.copyURLToFile(url, new File(parent, child));
    }

    private DataStore getDataStoreConnection(File file) throws Exception {
        Map<String, Object> connect = Maps.newHashMap();
        connect.put("url", file.toURI().toURL());
        connect.put("charset", Charset.forName("UTF-8"));

        return DataStoreFinder.getDataStore(connect);
    }
}
