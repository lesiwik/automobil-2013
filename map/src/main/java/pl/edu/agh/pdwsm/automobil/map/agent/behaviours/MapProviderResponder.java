package pl.edu.agh.pdwsm.automobil.map.agent.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.Serializable;
import java.util.logging.Level;

import pl.edu.agh.pdwsm.automobil.agent.behaviours.AbstractJadeBehaviour;
import pl.edu.agh.pdwsm.automobil.agent.behaviours.AbstractJadeMessageBehaviour;
import pl.edu.agh.pdwsm.automobil.map.MapProvider;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.MapQuery;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import edu.uci.ics.jung.graph.Graph;

public class MapProviderResponder extends AbstractJadeMessageBehaviour {

    private final MessageTemplate mapQueryTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchConversationId(AbstractJadeBehaviour.MAP_QUERY));

    private final MapProvider mapProvider;

    public MapProviderResponder(Agent agent, MapProvider mapProvider) {
        super(agent);
        this.mapProvider = mapProvider;
    }

    @Override
    protected MessageTemplate getMessageTemplate() {
        return mapQueryTemplate;
    }

    @Override
    protected void execMessage(ACLMessage msg) throws Exception {
        MapQuery mapQuery = (MapQuery) msg.getContentObject();
        Vertex startPoint = mapQuery.getStartPoint();
        Vertex endPoint = mapQuery.getEndPoint();
        Graph<Vertex, Edge> graph = mapProvider.getGraph(startPoint, endPoint);
        AID sender = msg.getSender();
        ACLMessage message = createMessage(ACLMessage.INFORM, AbstractJadeBehaviour.MAP_RESPONSE);
        message.addReceiver(sender);
        message.setContentObject((Serializable) graph);
        myAgent.send(message);

        logger.log(Level.INFO, "Responding with info to: " + sender + " with: " + graph);
    }
}
