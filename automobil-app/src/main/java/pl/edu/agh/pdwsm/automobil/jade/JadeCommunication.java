package pl.edu.agh.pdwsm.automobil.jade;

import android.location.Criteria;
import jade.android.AndroidHelper;
import jade.android.MicroRuntimeService;
import jade.android.MicroRuntimeServiceBinder;
import jade.android.RuntimeCallback;
import jade.core.MicroRuntime;
import jade.core.Profile;
import jade.util.Logger;
import jade.util.leap.Properties;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;

import java.util.UUID;
import java.util.logging.Level;

import org.joda.time.DateTime;

import pl.edu.agh.pdwsm.automobil.R;
import pl.edu.agh.pdwsm.automobil.UpdateRouteService;
import pl.edu.agh.pdwsm.automobil.agent.MobileAgent;
import pl.edu.agh.pdwsm.automobil.agent.NavigationAgent;
import pl.edu.agh.pdwsm.automobil.ontology.Position;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;

public class JadeCommunication {
    private MicroRuntimeServiceBinder microRuntimeServiceBinder;
    private final Logger logger = Logger.getJADELogger(this.getClass().getName());
    private final Context context;
    private final String nickname = Build.MODEL + "-" + UUID.randomUUID().toString();
    private MobileAgent mobileAgent;

    private static JadeCommunication INSTANCE;

    private JadeCommunication(Context context) {
        this.context = context;
    }

    public static void createInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new JadeCommunication(context);
            INSTANCE.connect();
        }
    }

    public static JadeCommunication getInstance() {
        return INSTANCE;
    }

    public MobileAgent getMobileAgent() {
        return mobileAgent;
    }

    private void startGPS() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        String bestProvider = locationManager.getBestProvider(new Criteria(), true);
        Location lastKnownLocation = locationManager.getLastKnownLocation(bestProvider);
        updateAgentsPosition(lastKnownLocation);
        Looper.prepare();
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onLocationChanged(Location location) {
                updateAgentsPosition(location);
                Intent intent = new Intent(context, UpdateRouteService.class);
                context.startService(intent);
            }
        });
    }

    private void updateAgentsPosition(Location location) {
        if (mobileAgent != null)
            mobileAgent.updatePosition(new Position(location.getLatitude(), location.getLongitude(), new DateTime(location
                    .getTime()), location.getSpeed()));
    }

    final RuntimeCallback<AgentController> agentStartupCallback = new RuntimeCallback<AgentController>() {
        @Override
        public void onSuccess(AgentController agent) {
            try {
                mobileAgent = agent.getO2AInterface(MobileAgent.class);
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
            startGPS();
        }

        @Override
        public void onFailure(Throwable throwable) {
            logger.log(Level.INFO, "Nickname already in use!");
        }
    };

    public void connect() {
        SharedPreferences settings = context.getSharedPreferences(context.getString(R.string.preferences), 0);
        String host = settings.getString(context.getString(R.string.hostKey), "");
        String port = settings.getString(context.getString(R.string.portKey), "");
        connectToPlatform(host, port, nickname);
    }

    public void reconnect() {
        disconnect();
        connect();
    }

    private void connectToPlatform(String host, String port, final String nickname) {
        final Properties profile = new Properties();
        profile.setProperty(Profile.MAIN_HOST, host);
        profile.setProperty(Profile.MAIN_PORT, port);
        profile.setProperty(Profile.MAIN, Boolean.FALSE.toString());
        profile.setProperty(Profile.JVM, Profile.ANDROID);

        if (AndroidHelper.isEmulator()) {
            // Emulator: this is needed to work with emulated devices
            profile.setProperty(Profile.LOCAL_HOST, AndroidHelper.LOOPBACK);
        } else {
            profile.setProperty(Profile.LOCAL_HOST, AndroidHelper.getLocalIPAddress());
        }
        // Emulator: this is not really needed on a real device
        profile.setProperty(Profile.LOCAL_PORT, port);

        if (microRuntimeServiceBinder == null) {
            ServiceConnection serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName className, IBinder service) {
                    microRuntimeServiceBinder = (MicroRuntimeServiceBinder) service;
                    logger.log(Level.INFO, "Gateway successfully bound to MicroRuntimeService");
                    startContainer(nickname, profile, agentStartupCallback);
                }

                @Override
                public void onServiceDisconnected(ComponentName className) {
                    microRuntimeServiceBinder = null;
                    logger.log(Level.INFO, "Gateway unbound from MicroRuntimeService");
                }
            };
            logger.log(Level.INFO, "Binding Gateway to MicroRuntimeService...");
            context.bindService(new Intent(context, MicroRuntimeService.class), serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            logger.log(Level.INFO, "MicroRumtimeGateway already binded to service");
            startContainer(nickname, profile, agentStartupCallback);
        }
    }

    private void startContainer(final String nickname, Properties profile, final RuntimeCallback<AgentController> agentStartupCallback) {
        if (!MicroRuntime.isRunning()) {
            microRuntimeServiceBinder.startAgentContainer(profile, new RuntimeCallback<Void>() {
                @Override
                public void onSuccess(Void thisIsNull) {
                    logger.log(Level.INFO, "Successfully start of the container...");
                    startAgent(nickname, agentStartupCallback);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    logger.log(Level.SEVERE, "Failed to start the container...");
                }
            });
        } else {
            startAgent(nickname, agentStartupCallback);
        }
    }

    private void startAgent(final String nickname, final RuntimeCallback<AgentController> agentStartupCallback) {
        microRuntimeServiceBinder.startAgent(nickname, NavigationAgent.class.getName(), new Object[] { context },
                new RuntimeCallback<Void>() {
                    @Override
                    public void onSuccess(Void thisIsNull) {
                        logger.log(Level.INFO, "Successfully start of the " + NavigationAgent.class.getName() + "...");
                        try {
                            agentStartupCallback.onSuccess(MicroRuntime.getAgent(nickname));
                        } catch (ControllerException e) {
                            // Should never happen
                            agentStartupCallback.onFailure(e);
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        logger.log(Level.SEVERE, "Failed to start the " + NavigationAgent.class.getName() + "...");
                        agentStartupCallback.onFailure(throwable);
                    }
                });
    }

    public void disconnect() {
        logger.log(Level.INFO, "Stopping Jade...");
        microRuntimeServiceBinder.stopAgentContainer(new RuntimeCallback<Void>() {
            @Override
            public void onSuccess(Void thisIsNull) {
            }

            @Override
            public void onFailure(Throwable throwable) {
                logger.log(Level.SEVERE, "Failed to stop the " + NavigationAgent.class.getName() + "...");
                agentStartupCallback.onFailure(throwable);
            }
        });
    }

}
