package pl.edu.agh.pdwsm.automobil;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import pl.edu.agh.pdwsm.automobil.agent.MobileAgent;
import pl.edu.agh.pdwsm.automobil.jade.JadeCommunication;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class UpdateRouteService extends IntentService {

    private static final String CLASS_NAME = "UpdateRouteService";

    public UpdateRouteService() {
        super(CLASS_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        MobileAgent mobileAgent = JadeCommunication.getInstance().getMobileAgent();
        if (mobileAgent != null) {
            List<Edge> path = mobileAgent.currentPath();
            List<LatLng> nodesList = convertEdgeListToNodes(path);

            AutomobilClientApplication.LAST_PATH = AutomobilClientApplication.PATH;
            AutomobilClientApplication.PATH = nodesList;

            updateRouteToActivity();
            sendNotification();
            stopSelf();
        }
    }

    private List<LatLng> convertEdgeListToNodes(List<Edge> edgeList) {

        List<LatLng> nodesList = new ArrayList<LatLng>(edgeList.size() + 1);

        LatLng startPoint = null;
        LatLng endPoint = null;
        for (Edge edge : edgeList) {
            Vertex startVertex = edge.getStartPoint();
            Vertex endVertex = edge.getEndPoint();
            startPoint = new LatLng(startVertex.getLatitude(), startVertex.getLongitude());

            if (endPoint != null && !startPoint.equals(endPoint)) {
                Log.w(CLASS_NAME, "Start point and end point are not equal");
                nodesList.add(endPoint);
            }
            nodesList.add(startPoint);

            endPoint = new LatLng(endVertex.getLatitude(), endVertex.getLongitude());
        }
        nodesList.add(endPoint);
        return nodesList;
    }

    private void updateRouteToActivity() {
        Intent intent = new Intent(Constants.UPDATE_ROUTE_FILTER);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendNotification() {
        System.out.println("NOTIFICATION");
        Intent intent = new Intent(this, RouteActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification noti = new NotificationCompat.Builder(this).setContentTitle("AGH-automobil").setContentText("Route has changed")
                .setSmallIcon(R.drawable.ic_launcher).setContentIntent(pIntent).build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(Constants.NOTIFICATION_ROUTE_CHANGED_ID, noti);

    }

}
