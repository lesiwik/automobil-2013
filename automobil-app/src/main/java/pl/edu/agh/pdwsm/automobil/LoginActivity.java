package pl.edu.agh.pdwsm.automobil;

import pl.edu.agh.pdwsm.automobil.jade.JadeCommunication;
import pl.edu.agh.pdwsm.automobil.util.Constants;
import pl.edu.agh.pdwsm.automobil.util.SystemUiHider;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class LoginActivity extends Activity {
    static final int SETTINGS_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        createNotification();
        setupGPS();
        startService(new Intent(this, UpdateRouteService.class));
    }

    private void setupGPS() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void createNotification() {
        Intent intent = new Intent(this, RouteActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification noti = new NotificationCompat.Builder(this).setContentTitle("AGH-automobil").setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent).build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        noti.flags |= Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONGOING_EVENT;

        notificationManager.notify(Constants.NOTIFICATION_ID, noti);
    }

    public void openRouteMap(View v) {
        startActivity(new Intent(this, RouteActivity.class));
    }

    public void dummyChangeRoute(View v) {
        System.out.println("CHANGE ROUTE");
        // startService(new Intent(this, UpdateRouteService.class));
        Context ctx = getApplicationContext();
        Intent intent = new Intent(ctx, UpdateRouteService.class);
        ctx.startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings) {
            Intent showSettings = new Intent(LoginActivity.this, SettingsActivity.class);
            LoginActivity.this.startActivityForResult(showSettings, SETTINGS_REQUEST);
            return true;
        } else if (item.getItemId() == R.id.menu_exit) {
            finish();
        } else if (item.getItemId() == R.id.menu_is_privilaged) {
        	AutomobilClientApplication.IS_PRIVILEGED = !AutomobilClientApplication.IS_PRIVILEGED;
        	item.setChecked(AutomobilClientApplication.IS_PRIVILEGED);
        }
        
        
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The chat activity was closed.
        JadeCommunication.getInstance().disconnect();
    }

}
