package pl.edu.agh.pdwsm.automobil;

import jade.util.Logger;

import java.util.List;
import java.util.logging.Level;

import pl.edu.agh.pdwsm.automobil.jade.JadeCommunication;
import android.annotation.SuppressLint;
import android.app.Application;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

@SuppressLint("NewApi")
public class AutomobilClientApplication extends Application {
    private Logger logger = Logger.getJADELogger(this.getClass().getName());
    
    public static List<LatLng> PATH = null;
    
    public static List<LatLng> LAST_PATH = null;
    
    public static LatLng START_POINT = null;
    
    public static LatLng END_POINT = null;
    
    public static String DESTINATION_ADDRESS_TEXT  = "";
    
    public static CameraPosition CAMERA_POSITION = null;
    
    public static boolean IS_PRIVILEGED = false;

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferences settings = getSharedPreferences(getString(R.string.preferences), 0);

        String defaultHost = settings.getString(getString(R.string.hostKey), "");
        String defaultPort = settings.getString(getString(R.string.portKey), "");
        if (defaultHost.isEmpty() || defaultPort.isEmpty()) {
            logger.log(Level.INFO, "Create default properties");
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(getString(R.string.hostKey), getString(R.string.defaultHost));
            editor.putString(getString(R.string.portKey), getString(R.string.defaultPort));
            editor.commit();
        }
        JadeCommunication.createInstance(getApplicationContext());
    }

}
