package pl.edu.agh.pdwsm.automobil;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import pl.edu.agh.pdwsm.automobil.jade.JadeCommunication;

public class SettingsActivity extends Activity {
    EditText hostField;
    EditText portField;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = getSharedPreferences(getString(R.string.preferences), 0);

        String host = settings.getString(getString(R.string.hostKey), "");
        String port = settings.getString(getString(R.string.portKey), "");

        setContentView(R.layout.settings);

        hostField = (EditText) findViewById(R.id.edit_host);
        hostField.setText(host);

        portField = (EditText) findViewById(R.id.edit_port);
        portField.setText(port);

        Button button = (Button) findViewById(R.id.button_use);
        button.setOnClickListener(buttonUseListener);
    }

    private OnClickListener buttonUseListener = new OnClickListener() {
        public void onClick(View v) {
            SharedPreferences settings = getSharedPreferences(getString(R.string.preferences), 0);

            // TODO: Verify that edited parameters was formally correct
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(getString(R.string.hostKey), hostField.getText().toString());
            editor.putString(getString(R.string.portKey), portField.getText().toString());
            editor.commit();

            finish();
            JadeCommunication.getInstance().reconnect();
        }
    };
}
