package pl.edu.agh.pdwsm.automobil;

import java.io.IOException;
import java.util.List;

import org.joda.time.DateTime;

import pl.edu.agh.pdwsm.automobil.agent.MobileAgent;
import pl.edu.agh.pdwsm.automobil.jade.JadeCommunication;
import pl.edu.agh.pdwsm.automobil.ontology.Position;
import pl.edu.agh.pdwsm.automobil.util.Constants;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class RouteActivity extends FragmentActivity implements
		OnMapClickListener {

	private GoogleMap mMap;
	private EditText destAddressText;
	private Marker actualDestMarker;

	private Marker actualSourceMarker;

	private Polyline lastPoly;
	private Polyline actualPoly;

	private Geocoder geocoder;

	private final BroadcastReceiver updateRouteReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			System.out.println("Route Has Changed");
			showToast("Route Has Changed");
			setRoute(AutomobilClientApplication.PATH);
			resetCamera();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_route);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				updateRouteReceiver,
				new IntentFilter(Constants.UPDATE_ROUTE_FILTER));

		destAddressText = (EditText) findViewById(R.id.destination_address);
		geocoder = new Geocoder(this);

		CameraPosition cameraPosition = AutomobilClientApplication.CAMERA_POSITION;
		LatLng endMarkerPosition = AutomobilClientApplication.END_POINT;
		LatLng startMarkerPositon = AutomobilClientApplication.START_POINT;

		destAddressText
				.setText(AutomobilClientApplication.DESTINATION_ADDRESS_TEXT);

		setUpMapIfNeeded(cameraPosition, endMarkerPosition, startMarkerPositon);

		destAddressText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {

					new GetLatLngFromAddressTask().execute(destAddressText
							.getText().toString());
					return true;
				}
				return false;
			}
		});
		System.out.println("CREATE");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.route_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.menu_current_location) {
			actualSourceMarker.setPosition(getCurrentLocation());
			resetCamera();
		}
		return super.onOptionsItemSelected(item);

	}

	private LatLng getCurrentLocation() {
		LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String towers = mlocManager.getBestProvider(criteria, false);
		Location currentLocation = mlocManager.getLastKnownLocation(towers);
		if (currentLocation == null) {
			showToast("Unable to find current location");
			return new LatLng(0, 0);
		} else {
			return new LatLng(currentLocation.getLatitude(),
					currentLocation.getLongitude());
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		System.out.println("RESUME");
	}

	@Override
	protected void onPause() {
		super.onPause();
		AutomobilClientApplication.END_POINT = actualDestMarker.getPosition();
		AutomobilClientApplication.DESTINATION_ADDRESS_TEXT = destAddressText
				.getText().toString();
		AutomobilClientApplication.CAMERA_POSITION = mMap.getCameraPosition();
		AutomobilClientApplication.START_POINT = actualSourceMarker
				.getPosition();
	}

	private void setUpMapIfNeeded(CameraPosition cameraPosition,
			LatLng markerPosition, LatLng startMarkerPosition) {

		if (mMap == null) {
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap(cameraPosition, markerPosition, startMarkerPosition);
			}
		}
	}

	/**
	 * This is where we can add markers or lines, add listeners or move the
	 * camera. In this case, we just add a marker near Africa.
	 * <p>
	 * This should only be called once and when we are sure that {@link #mMap}
	 * is not null.
	 */
	private void setUpMap(CameraPosition cameraPosition, LatLng markerPosition,
			LatLng startMarkerPosition) {
		mMap.setOnMapClickListener(this);

		if (markerPosition == null) {
			markerPosition = getCoordinatesFromAddress("Polska");
			if (markerPosition == null) {
				markerPosition = new LatLng(0, 0);
			}
		}

		if (startMarkerPosition == null) {
			startMarkerPosition = getCurrentLocation();
		}
		actualDestMarker = mMap.addMarker(new MarkerOptions().position(
				markerPosition).title("Cel"));

		actualSourceMarker = mMap.addMarker(new MarkerOptions()
				.position(startMarkerPosition).title("poczatek")
				.draggable(true));

		if (AutomobilClientApplication.LAST_PATH != null) {
			setRoute(AutomobilClientApplication.LAST_PATH);
		}
		if (AutomobilClientApplication.PATH != null) {
			setRoute(AutomobilClientApplication.PATH);
		}

		if (cameraPosition == null) {
			cameraPosition = CameraPosition.fromLatLngZoom(
					actualSourceMarker.getPosition(), 5);
		}
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
				cameraPosition.target, cameraPosition.zoom));
	}

	protected void setRoute(List<LatLng> pointList) {

		if (lastPoly != null) {
			lastPoly.remove();
		}

		if (actualPoly != null) {
			lastPoly = actualPoly;
			lastPoly.setColor(Color.GREEN);
			lastPoly.setWidth(10);
		}

		actualPoly = mMap.addPolyline(new PolylineOptions().addAll(pointList)
				.width(5).color(Color.RED));

	}

	private void resetCamera() {
		LatLngBounds.Builder bc = new LatLngBounds.Builder();
		bc.include(actualSourceMarker.getPosition());
		bc.include(actualDestMarker.getPosition());

		mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
	}

	@Override
	public void onMapClick(LatLng point) {
		new GetGeoloctionTask().execute(point);
		setMarker(point);
	}

	private void setMarker(LatLng point) {
		actualDestMarker.setPosition(point);

		mMap.animateCamera(CameraUpdateFactory.newLatLng(actualDestMarker
				.getPosition()));
	}

	private LatLng getCoordinatesFromAddress(String address) {
		try {
			List<Address> geoResults = geocoder.getFromLocationName(address, 1);
			if (!geoResults.isEmpty()) {
				return new LatLng(geoResults.get(0).getLatitude(), geoResults
						.get(0).getLongitude());
			} else {
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void showToast(String text) {
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

	public void dummyChangeRoute(View v) {
//		System.out.println("CHANGE ROUTE2");
//		startService(new Intent(this, UpdateRouteService.class));

		// TODO uncomment
		MobileAgent mobileAgent = JadeCommunication.getInstance()
				.getMobileAgent();
		mobileAgent.startRouting(
				new Position(actualSourceMarker.getPosition().latitude,
						actualSourceMarker.getPosition().longitude,
						new DateTime()),
				new Position(actualDestMarker.getPosition().latitude,
						actualDestMarker.getPosition().longitude,
						new DateTime()),
				AutomobilClientApplication.IS_PRIVILEGED);
	}

	private class GetGeoloctionTask extends AsyncTask<LatLng, String, String> {

		@Override
		protected String doInBackground(LatLng... params) {
			LatLng coordinates = params[0];
			String result = null;
			try {
				List<Address> addresses = geocoder.getFromLocation(
						coordinates.latitude, coordinates.longitude, 1);
				if (!addresses.isEmpty()) {
					Address address = addresses.get(0);
					result = "";
					for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
						result += address.getAddressLine(i) + " ";
					}
				}
				return result;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(String address) {
			if (address != null) {
				destAddressText.setText(address);
			} else {
				destAddressText.setText("Nieznany adres");
				showToast("Nieznany adres");
			}
		}

	}

	private class GetLatLngFromAddressTask extends
			AsyncTask<String, String, LatLng> {

		@Override
		protected LatLng doInBackground(String... params) {
			String address = params[0];		
			return getCoordinatesFromAddress(address);

		}

		@Override
		protected void onPostExecute(LatLng coordinates) {
			if (coordinates != null) {
				setMarker(coordinates);
			} else {
				showToast("Nieznany adres");
				destAddressText.setText("Nieznany adres");
			}

		}

	}

}
