package pl.edu.agh.pdwsm.automobil.util;

public final class Constants {
	
	public static final int NOTIFICATION_ID = 1111;
	
	public static final int NOTIFICATION_ROUTE_CHANGED_ID = 2111;
	
	public static final String UPDATE_ROUTE_FILTER = "updateRouteFilter";
	
	public static final String LAST_PATH = "lastPath";

}
