package pl.edu.agh.pdwsm.automobil;

import org.joda.time.DateTime;
import org.junit.Test;
import pl.edu.agh.pdwsm.automobil.ontology.DistanceCalculator;
import pl.edu.agh.pdwsm.automobil.ontology.Position;

import static org.junit.Assert.assertEquals;

public class DistanceCalculatorTest {

    @Test
    public void shouldCalculateDistance() {
        Position pos1 = new Position(50.06, 19.92, DateTime.now());
        Position pos2 = new Position(50.061, 19.921, DateTime.now());

        double distance = DistanceCalculator.calculateDistance(pos1, pos2);

        assertEquals(132, distance, 1);
    }

}
