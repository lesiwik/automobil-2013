package pl.edu.agh.pdwsm.automobil;

import jade.core.AID;
import jade.proto.SubscriptionResponder;
import pl.edu.agh.pdwsm.automobil.ontology.Position;

public class AgentInfo {
    private AID id;
    private Position position;
    private SubscriptionResponder.Subscription subscription;

    public AgentInfo(AID id, SubscriptionResponder.Subscription subscription) {
        this.id = id;
        this.subscription = subscription;
    }

    public AID getId() {
        return id;
    }

    public Position getPosition() {
        return position;
    }

    public SubscriptionResponder.Subscription getSubscription() {
        return subscription;
    }

    public void updatePosition(Position position) {
        this.position = position;
    }

}
