package pl.edu.agh.pdwsm.automobil;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.introspection.AMSSubscriber;
import jade.domain.introspection.DeadAgent;
import jade.domain.introspection.Event;
import jade.domain.introspection.IntrospectionOntology;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SubscriptionResponder;
import jade.proto.SubscriptionResponder.Subscription;
import jade.proto.SubscriptionResponder.SubscriptionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.pdwsm.automobil.ontology.AutomobilOntology;
import pl.edu.agh.pdwsm.automobil.ontology.Position;

import java.util.Collection;
import java.util.Map;

public class AgentManager extends Agent implements SubscriptionManager {

    private Codec codec = new SLCodec();
    private Ontology onto = AutomobilOntology.getInstance();
    private AMSSubscriber myAMSSubscriber;

    private final Logger logger = LoggerFactory.getLogger(AgentManager.class);
    private AgentsContainer agentsContainer = new AgentsContainer();

    protected void setup() {
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(onto);

        MessageTemplate sTemplate = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.SUBSCRIBE),
                MessageTemplate.and(
                        MessageTemplate.MatchLanguage(codec.getName()),
                        MessageTemplate.MatchOntology(onto.getName())));
        addBehaviour(new SubscriptionResponder(this, sTemplate, this));

        // Register to the AMS to detect when chat participants suddenly die
        myAMSSubscriber = new AMSSubscriber() {

            @SuppressWarnings("unchecked")
            protected void installHandlers(Map handlersTable) {
                handlersTable.put(IntrospectionOntology.DEADAGENT, new EventHandler() {

                    public void handle(Event ev) {
                        DeadAgent da = (DeadAgent) ev;
                        AID id = da.getAgent();
                        if (agentsContainer.containsAgent(id)) {
                            deregister(agentsContainer.getSubscription(id));
                        }
                    }
                });
            }
        };
        addBehaviour(myAMSSubscriber);

        addBehaviour(new PositionUpdateReceiver());
    }

    class PositionUpdateReceiver extends CyclicBehaviour {
        MessageTemplate positionTemplate = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                MessageTemplate.MatchConversationId("position"));

        @Override
        public void action() {
            ACLMessage msg = myAgent.receive(positionTemplate);
            if (msg != null && agentsContainer.containsAgent(msg.getSender())) {
                try {
                    Position position = (Position) myAgent.getContentManager().extractContent(msg);
                    logger.info("Agent {}: position update: {}", msg.getSender(), position);
                    agentsContainer.updatePosition(msg.getSender(), position);
                    ACLMessage reply = msg.createReply();
                    Collection<AgentInfo> closeAgents = agentsContainer.findCloseAgents(msg.getSender());
                    for (AgentInfo closeAgent : closeAgents) {
                        ACLMessage clone = (ACLMessage) reply.clone();
                        clone.addReceiver(closeAgent.getId());
                        closeAgent.getSubscription().notify(clone);
                    }
                } catch (Exception e) {
                    logger.error("Position update error", e);
                }
            } else {
                block();
            }

        }
    }

    protected void takeDown() {
        send(myAMSSubscriber.getCancel());
    }

    @Override
    public boolean register(Subscription s) throws RefuseException, NotUnderstoodException {
        AID newId = s.getMessage().getSender();
        logger.info("Registering new agent: " + newId);
        agentsContainer.addAgent(newId, s);
        return false;
    }

    @Override
    public boolean deregister(Subscription s) {
        AID oldId = s.getMessage().getSender();
        logger.info("Unregistering agent: " + oldId);
        agentsContainer.removeAgent(oldId);
        return false;
    }
}
