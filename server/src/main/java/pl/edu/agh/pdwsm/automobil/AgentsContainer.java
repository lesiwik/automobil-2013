package pl.edu.agh.pdwsm.automobil;

import static com.google.common.collect.Maps.filterEntries;
import jade.core.AID;
import jade.proto.SubscriptionResponder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.pdwsm.automobil.ontology.DistanceCalculator;
import pl.edu.agh.pdwsm.automobil.ontology.Position;

import com.google.common.base.Predicate;

public class AgentsContainer {
    private final Map<AID, AgentInfo> agentsMap = new HashMap<AID, AgentInfo>();
    private final double maxDistance = 300;

    public boolean containsAgent(AID agentId) {
        return agentsMap.containsKey(agentId);
    }

    public SubscriptionResponder.Subscription getSubscription(AID agentId) {
        return agentsMap.get(agentId).getSubscription();
    }

    public void addAgent(AID agentId, SubscriptionResponder.Subscription subscription) {
        agentsMap.put(agentId, new AgentInfo(agentId, subscription));
    }

    public void removeAgent(AID agentId) {
        agentsMap.remove(agentId);
    }

    public void updatePosition(AID agentId, Position position) {
        AgentInfo agentInfo = agentsMap.get(agentId);
        if (agentInfo != null)
            agentInfo.updatePosition(position);
    }

    public Collection<AgentInfo> findCloseAgents(final AID agentId) {
        final Position referencePosition = agentsMap.get(agentId).getPosition();
        return filterEntries(agentsMap, new Predicate<Map.Entry<AID, AgentInfo>>() {
            @Override
            public boolean apply(Map.Entry<AID, AgentInfo> input) {
                Position position = input.getValue().getPosition();
                if (position == null) {
                    return false;
                }
                double distance = DistanceCalculator.calculateDistance(position, referencePosition);
                return !input.getKey().equals(agentId) && distance < maxDistance;
            }
        }).values();
    }
}
