package pl.edu.agh.pdwsm.automobil.ontology;

import jade.content.onto.BasicOntology;
import jade.content.onto.CFReflectiveIntrospector;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.PredicateSchema;

public class AutomobilOntology extends Ontology {
    // ONTOLOGY NAME
    public static final String ONTOLOGY_NAME = "Automobil-ontology";

    // VOCABULARY
    public static final String POSITION = "position";
    public static final String POSITION_LAT = "latitude";
    public static final String POSITION_LON = "longitude";

    private static Ontology theInstance = new AutomobilOntology();

    public static Ontology getInstance() {
        return theInstance;
    }

    private AutomobilOntology() {
        super(ONTOLOGY_NAME, BasicOntology.getInstance(), new CFReflectiveIntrospector());

        try {
            add(new PredicateSchema(POSITION), Position.class);

            PredicateSchema ps = (PredicateSchema) getSchema(POSITION);
            ps.add(POSITION_LAT, getSchema(BasicOntology.FLOAT));
            ps.add(POSITION_LON, getSchema(BasicOntology.FLOAT));

        } catch (OntologyException oe) {
            oe.printStackTrace();
        }
    }
}
