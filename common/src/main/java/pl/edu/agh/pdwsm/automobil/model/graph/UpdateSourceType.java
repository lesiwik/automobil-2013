package pl.edu.agh.pdwsm.automobil.model.graph;

public enum UpdateSourceType {
    NORMAL, PRIVILEGED;
}
