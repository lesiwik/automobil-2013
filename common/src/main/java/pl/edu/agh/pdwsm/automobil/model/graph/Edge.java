package pl.edu.agh.pdwsm.automobil.model.graph;

import java.io.Serializable;

import org.joda.time.DateTime;

import pl.edu.agh.pdwsm.automobil.ontology.Position;

public class Edge implements Serializable {
    private static final long serialVersionUID = 1L;
    private double weight;
    private final Vertex startPoint;
    private final Vertex endPoint;
    private DateTime timestamp = DateTime.now();
    private final double length;
    private final int perfectSpeed;
    private UpdateSourceType lastUpdateType;

    public Edge(Vertex startPoint, Vertex endPoint, double length, int perfectSpeed) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.length = length;
        this.perfectSpeed = perfectSpeed;
        this.weight = length / perfectSpeed;
        lastUpdateType = UpdateSourceType.NORMAL;
    }

    public boolean isPointOnEdge(Position point) {
        double x = (startPoint.getLongitude() - point.getLongitude()) * (endPoint.getLatitude() - point.getLatitude())
                - (startPoint.getLatitude() - point.getLatitude()) * (endPoint.getLongitude() - point.getLongitude());
        double minLat = Math.min(startPoint.getLatitude(), endPoint.getLatitude());
        double maxLat = Math.max(startPoint.getLatitude(), endPoint.getLatitude());
        double minLon = Math.min(startPoint.getLongitude(), endPoint.getLongitude());
        double maxLon = Math.max(startPoint.getLongitude(), endPoint.getLongitude());
        return point.getLatitude() >= minLat && point.getLatitude() <= maxLat && point.getLongitude() >= minLon
                && point.getLongitude() <= maxLon &&
                // should be enough
                Math.abs(x) < 1e-10;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double segmentTravelTime) {
        this.weight = segmentTravelTime;
    }

    public Vertex getStartPoint() {
        return startPoint;
    }

    public Vertex getEndPoint() {
        return endPoint;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }

    public double getLength() {
        return length;
    }

    public int getPerfectSpeed() {
        return perfectSpeed;
    }

    public UpdateSourceType getLastUpdateType() {
        return lastUpdateType;
    }

    public void setLastUpdateType(UpdateSourceType lastUpdateType) {
        this.lastUpdateType = lastUpdateType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((endPoint == null) ? 0 : endPoint.hashCode());
        result = prime * result + ((startPoint == null) ? 0 : startPoint.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Edge other = (Edge) obj;
        if (endPoint == null) {
            if (other.endPoint != null)
                return false;
        } else if (!endPoint.equals(other.endPoint))
            return false;
        if (startPoint == null) {
            if (other.startPoint != null)
                return false;
        } else if (!startPoint.equals(other.startPoint))
            return false;
        return true;
    }
}
