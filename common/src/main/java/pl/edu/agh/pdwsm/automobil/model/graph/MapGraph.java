package pl.edu.agh.pdwsm.automobil.model.graph;

import com.google.common.base.Objects;

import edu.uci.ics.jung.graph.DirectedSparseGraph;

public class MapGraph extends DirectedSparseGraph<Vertex, Edge> {

	private final Vertex startPoint;
	private final Vertex endPoint;

	public MapGraph(Vertex startPoint, Vertex endPoint) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this). //
				add("start point", startPoint). //
				add("end point", endPoint). //
				add("vertices", getVertexCount()). //
				add("edges", getEdgeCount()). //
				toString();
	}
}
