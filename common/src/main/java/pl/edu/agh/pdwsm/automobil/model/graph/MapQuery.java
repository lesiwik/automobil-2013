package pl.edu.agh.pdwsm.automobil.model.graph;

import jade.content.Predicate;

import java.io.Serializable;

public class MapQuery implements Predicate, Serializable {

    private static final long serialVersionUID = -1483359133163990665L;

    private final Vertex startPoint;

    private final Vertex endPoint;

    public MapQuery(Vertex startPoint, Vertex endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    public Vertex getStartPoint() {
        return startPoint;
    }

    public Vertex getEndPoint() {
        return endPoint;
    }
}
