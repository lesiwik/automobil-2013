package pl.edu.agh.pdwsm.automobil.model.graph;

import pl.edu.agh.pdwsm.automobil.ontology.Position;

public final class Vertices {

	private Vertices() {

	}

	public static Vertex newVertex(double latitude, double longitude) {
		return new PreciseVertex(latitude, longitude);
	}

	public static Vertex newVertex(Position position) {
		return new PreciseVertex(position);
	}


	public static ApproxVertexBuilder approximation(int digitsAfterDot) {
		return new ApproxVertexBuilder(digitsAfterDot);
	}

	public static class ApproxVertexBuilder {

		private final int digitsAfterDot;

		public ApproxVertexBuilder(int digitsAfterDot) {
			this.digitsAfterDot = digitsAfterDot;
		}

		public Vertex newVertex(double latitude, double longitude) {
			double reduceLatitude = reducePrecision(latitude);
			double reduceLongitude = reducePrecision(longitude);
			
			return new PreciseVertex(reduceLatitude, reduceLongitude);
		}

		public Vertex newVertex(Position position) {
			return newVertex(position.getLatitude(), position.getLongitude());
		}
		
		public Vertex newVertex(Vertex vertex) {
			double reduceLatitude = reducePrecision(vertex.getLatitude());
			double reduceLongitude = reducePrecision(vertex.getLongitude());
			
			return new PreciseVertex(reduceLatitude, reduceLongitude);
		}
		
		public double reducePrecision(double number) {
			StringBuilder sb = new StringBuilder(String.valueOf(number));
			int dotIndex = sb.indexOf(".") + 1;
			
			int end = dotIndex + digitsAfterDot;
			if (end > sb.length()) {
				end = sb.length();
			}
			
			String reduceDouble = sb.substring(0, end);
			return Double.valueOf(reduceDouble);
		}
	}
}
