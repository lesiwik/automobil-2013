package pl.edu.agh.pdwsm.automobil.ontology;

import jade.content.Predicate;

import java.io.Serializable;

import org.joda.time.DateTime;

public class Position implements Predicate, Serializable {
    private double latitude;
    private double longitude;
    private double speed = 0.0;
    private DateTime timestamp;

    public Position() {
    }

    public Position(double latitude, double longitude, DateTime timestamp, double speed) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.setTimestamp(timestamp);
        this.speed = speed;
    }

    public Position(double latitude, double longitude, DateTime timestamp) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.setTimestamp(timestamp);
    }

    public Position shift(double latitude, double longitude, DateTime timestamp) {
        return new Position(this.latitude + latitude, this.longitude + longitude, timestamp, speed);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Position{" + "latitude=" + latitude + ", longitude=" + longitude + '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Position other = (Position) obj;
        if (Double.doubleToLongBits(latitude) != Double.doubleToLongBits(other.latitude))
            return false;
        if (Double.doubleToLongBits(longitude) != Double.doubleToLongBits(other.longitude))
            return false;
        return true;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }
}
