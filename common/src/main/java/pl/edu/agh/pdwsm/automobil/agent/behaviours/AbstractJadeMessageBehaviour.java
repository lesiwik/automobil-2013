package pl.edu.agh.pdwsm.automobil.agent.behaviours;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.logging.Level;

public abstract class AbstractJadeMessageBehaviour extends AbstractJadeBehaviour {

	protected AbstractJadeMessageBehaviour(Agent agent) {
		super(agent);
	}

	@Override
	public void action() {
		MessageTemplate messageTemplate = getMessageTemplate();
		ACLMessage message = myAgent.receive(messageTemplate);

		try {
			if (message != null) {
				execMessage(message);
			} else {
				block();
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, e.getMessage());
		}
	}

	protected abstract MessageTemplate getMessageTemplate();

	protected abstract void execMessage(ACLMessage msg) throws Exception;
}
