package pl.edu.agh.pdwsm.automobil.model.graph;

import org.joda.time.DateTime;

import pl.edu.agh.pdwsm.automobil.ontology.Position;

import com.google.common.base.Objects;

public class PreciseVertex implements Vertex {
    private static final long serialVersionUID = 387452067577756375L;

    private final double latitude;
    private final double longitude;

    public PreciseVertex(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public PreciseVertex(Position position) {
        this.latitude = position.getLatitude();
        this.longitude = position.getLongitude();
    }

    @Override
    public Position toPosition() {
        return new Position(latitude, longitude, DateTime.now());
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        PreciseVertex vertex = (PreciseVertex) o;

        return Double.compare(vertex.latitude, latitude) == 0 && Double.compare(vertex.longitude, longitude) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this).//
                add("latitude", latitude). //
                add("longitude", longitude). //
                toString();
    }
}
