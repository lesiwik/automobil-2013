package pl.edu.agh.pdwsm.automobil.agent.behaviours;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.Logger;
import pl.edu.agh.pdwsm.automobil.ontology.AutomobilOntology;

public abstract class AbstractJadeBehaviour extends CyclicBehaviour {

    private final Codec codec = new SLCodec();
    private final Ontology onto = AutomobilOntology.getInstance();

    protected static final String AGENT_MANAGER = "manager";
    protected static final String MAP_PROVIDER = "mapProvider";
    protected static final String TRAFFIC_INFO_QUERY = "traffic-query";
    protected static final String TRAFFIC_INFO_RESPONSE = "traffic-response";
    protected static final String MAP_QUERY = "map-query";
    protected static final String MAP_RESPONSE = "map-response";
    protected final Logger logger = Logger.getJADELogger(this.getClass().getName());

    protected AbstractJadeBehaviour(Agent a) {
        super(a);
    }

    protected ACLMessage createMessage(int type, String conversationId) {
        ACLMessage message = new ACLMessage(type);
        message.setLanguage(codec.getName());
        message.setOntology(onto.getName());
        message.setConversationId(conversationId);
        return message;
    }
}
