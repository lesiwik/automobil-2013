package pl.edu.agh.pdwsm.automobil.model.graph;

import java.io.Serializable;

import pl.edu.agh.pdwsm.automobil.ontology.Position;

public interface Vertex extends Serializable {

	Position toPosition();

	double getLatitude();

	double getLongitude();
}
