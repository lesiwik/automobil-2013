package pl.edu.agh.pdwsm.automobil.simulator.core;

import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Lists.transform;
import static pl.edu.agh.pdwsm.automobil.ontology.DistanceCalculator.calculateDistance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.DefaultListModel;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;

import pl.edu.agh.pdwsm.automobil.agent.MobileAgent;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.ontology.Position;
import pl.edu.agh.pdwsm.automobil.simulator.CracowVertices;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

public class SimulationController {
    private final Period stepPeriod;
    private final List<AgentModel> agents;
    private DateTime currentTime;
    private final int currentStepNumber;
    private final Collection<Vertex> vertices;
    private final Map<AgentModel, RoutePlan> plannedRoutes;
    private final Random random = new Random();
    private final Duration carStopPeriod;

    SimulationController(List<AgentModel> agents, Period stepPeriod, Duration carStopPeriod) {
        this.carStopPeriod = carStopPeriod;
        this.currentStepNumber = 0;
        this.stepPeriod = stepPeriod;
        this.currentTime = DateTime.now();
        this.plannedRoutes = Maps.newHashMap();

        vertices = CracowVertices.getVertices();
        this.agents = new ArrayList<AgentModel>();
        for (AgentModel agent : agents) {
            this.agents.add(agent);
        }
    }

    public DefaultListModel<AgentModel> getAgentModel() {
        DefaultListModel<AgentModel> agentModel = new DefaultListModel<AgentModel>();

        for (AgentModel agent : agents) {
            agentModel.addElement(agent);
        }

        return agentModel;
    }

    public List<AgentModel> getAgents() {
        return ImmutableList.copyOf(this.agents);
    }

    public List<Position> getAgentPositions() {
        return transform(agents, new Function<MobileAgent, Position>() {
            @Override
            public Position apply(MobileAgent input) {
                return input.currentPosition();
            }
        });
    }

    public DateTime getTime() {
        return this.currentTime;
    }

    public int getStepNumber() {
        return currentStepNumber;
    }

    void updateTime() {
        this.currentTime = this.currentTime.plus(stepPeriod);
    }

    public void registerRoute(AgentModel agent, Position from, Position to) {
        this.plannedRoutes.put(agent, RoutePlan.create(from, to));
    }

    public void nextStep(AgentModel agent) {
        if (!agent.isNavigating()) {
            startAgentIfInactiveForTooLong(agent);
        } else {
            double currentlyPassedDistance = agent.getSpeed() * stepPeriod.getSeconds();
            List<Edge> currentPath = newLinkedList(agent.currentPath());
            Position currentPosition = agent.currentPosition();
            DateTime newTimestamp = currentPosition.getTimestamp().plusSeconds(stepPeriod.getSeconds());
            currentPosition = calculateNewPosition(currentlyPassedDistance, currentPath, currentPosition);
            currentPosition.setSpeed(agent.getSpeed());
            currentPosition.setTimestamp(newTimestamp);
            agent.updatePosition(currentPosition);
        }
    }

    private Position calculateNewPosition(double currentlyPassedDistance, List<Edge> currentPath, Position currentPosition) {
        Edge currentSegment = currentPath.remove(0);
        double remainingDistance;
        while (currentlyPassedDistance > (remainingDistance = calculateDistance(currentPosition, currentSegment.getEndPoint().toPosition()))) {
            currentlyPassedDistance -= remainingDistance;
            currentPosition = currentSegment.getEndPoint().toPosition();
            if (!currentPath.isEmpty()) {
                currentSegment = currentPath.remove(0);
            } else {
                break;
            }
        }
        currentPosition = calculateNewPositionOnSegment(currentlyPassedDistance, currentPosition, currentSegment);
        return currentPosition;
    }

    private Position calculateNewPositionOnSegment(double currentlyPassedDistance, Position currentPosition, Edge currentSegment) {
        double ratio = currentlyPassedDistance / currentSegment.getLength();
        double latitude = currentSegment.getEndPoint().getLatitude() - currentSegment.getStartPoint().getLatitude();
        latitude *= ratio;
        double longitude = currentSegment.getEndPoint().getLongitude() - currentSegment.getStartPoint().getLongitude();
        longitude *= ratio;
        currentPosition = currentPosition.shift(latitude, longitude, currentPosition.getTimestamp());
        return currentPosition;
    }

    private void startAgentIfInactiveForTooLong(AgentModel agent) {
        if (agent.isInactiveForTooLong(currentTime, carStopPeriod)) {
            RoutePlan routePlan = plannedRoutes.get(agent);
            if (routePlan == null) {
                int startPoint = random.nextInt(vertices.size());
                int endPoint = randomInt(vertices.size(), startPoint);
                agent.startRouting(Iterables.get(vertices, startPoint, null).toPosition(), Iterables.get(vertices, endPoint, null)
                        .toPosition(), agent.isPrivileged());
            } else {
                agent.startRouting(routePlan.from(), routePlan.to(), agent.isPrivileged());
            }
        }
    }

    private int randomInt(int max, int distinctFrom) {
        int rand;
        do {
            rand = random.nextInt(max);
        } while (rand == distinctFrom);
        return rand;
    }

    private static final class RoutePlan {
        private final Position from;
        private final Position to;

        public static RoutePlan create(Position from, Position to) {
            return new RoutePlan(from, to);
        }

        private RoutePlan(Position from, Position to) {
            this.from = from;
            this.to = to;
        }

        public Position from() {
            return from;
        }

        public Position to() {
            return to;
        }
    }
}
