package pl.edu.agh.pdwsm.automobil.simulator.gui.icons;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class IconSet {
	private static final Logger log = LoggerFactory.getLogger(IconSet.class);

	private static final int DefaultIconSize = 32;

	public static final ImageIcon Alarm = createImageIcon("alarm.png", DefaultIconSize);
	public static final ImageIcon Browser = createImageIcon("browser.png", DefaultIconSize);
	public static final ImageIcon Compass = createImageIcon("compass.png", DefaultIconSize);
	public static final ImageIcon Maps = createImageIcon("maps.png", DefaultIconSize);
	public static final ImageIcon Score = createImageIcon("score.png", DefaultIconSize);
	//public static final ImageIcon Go = createImageIcon("gowalla.png", DefaultIconSize);
	public static final ImageIcon Go = createImageIcon("play.png", DefaultIconSize);
	public static final ImageIcon Pause = createImageIcon("pause.png", DefaultIconSize);

	public static ImageIcon createIcon(String path) {
		try {
			return new ImageIcon(ImageIO.read(IconSet.class.getResourceAsStream(path)));
		} catch (Exception ex) {
			String logMessage = String.format("Unable to load %s resource", path);
			log.error(logMessage, ex);
			return null;
		}
	}

	public static ImageIcon scale(ImageIcon icon, int size) {
		Image scaledImage = getScaledImage(icon.getImage(), DefaultIconSize, DefaultIconSize);
		return new ImageIcon(scaledImage);
	}

	private static boolean hasDefaultSize(ImageIcon icon) {
		return icon.getIconHeight() == DefaultIconSize && icon.getIconWidth() == DefaultIconSize;
	}

	private static ImageIcon createImageIcon(String path, int iconSize) {
		ImageIcon icon = createIcon(path);
		if (icon != null && !hasDefaultSize(icon)) {
			Image scaledImage = getScaledImage(icon.getImage(), DefaultIconSize, DefaultIconSize);
			icon = new ImageIcon(scaledImage);
		}
		return icon;
	}

	private static Image getScaledImage(Image srcImg, int width, int height) {
		BufferedImage resizedImg = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
		Graphics2D g2 = resizedImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(srcImg, 0, 0, width, height, null);
		g2.dispose();
		return resizedImg;
	}
}
