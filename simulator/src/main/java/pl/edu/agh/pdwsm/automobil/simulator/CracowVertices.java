package pl.edu.agh.pdwsm.automobil.simulator;

import java.util.Collection;
import java.util.Collections;

import pl.edu.agh.pdwsm.automobil.map.CracowMapProviderService;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;

public final class CracowVertices {

    public static Collection<Vertex> getVertices() {
        try {
            return new CracowMapProviderService().getGraph(null, null).getVertices();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
