package pl.edu.agh.pdwsm.automobil.simulator.gui;

import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;
import org.jdesktop.swingx.painter.AbstractPainter;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.simulator.gui.icons.ColorSet;

import static com.google.common.collect.Lists.newArrayList;

public class AgentPainter extends AbstractPainter<JXMapViewer> {
	private WaypointRenderer renderer = new AgentWaypointRenderer();
	private Set<Waypoint> waypoints;
	private List<Edge> route;

	public AgentPainter() {
		setAntialiasing(true);
		setCacheable(false);
		waypoints = new HashSet<Waypoint>();
	}

	public Set<Waypoint> getWaypoints() {
		return waypoints;
	}

	public void setWaypoints(Set<Waypoint> waypoints) {
		this.waypoints = waypoints;
	}

	public List<Edge> getRoute() {
		return route;
	}

	public void setRoute(List<Edge> route) {
        if(route != null) {
            route = newArrayList(route);
        }
        this.route = route;
    }

	@Override
	protected void doPaint(Graphics2D g, JXMapViewer map, int width, int height) {
		// figure out which waypoints are within this map viewport
		// so, get the bounds
		Rectangle viewportBounds = map.getViewportBounds();
		int zoom = map.getZoom();
		Dimension sizeInTiles = map.getTileFactory().getMapSize(zoom);
		int tileSize = map.getTileFactory().getTileSize(zoom);
		Dimension sizeInPixels = new Dimension(sizeInTiles.width * tileSize, sizeInTiles.height * tileSize);

		double vpx = viewportBounds.getX();
		// normalize the left edge of the viewport to be positive
		while (vpx < 0) {
			vpx += sizeInPixels.getWidth();
		}
		// normalize the left edge of the viewport to no wrap around the world
		while (vpx > sizeInPixels.getWidth()) {
			vpx -= sizeInPixels.getWidth();
		}

		Rectangle2D vp2 = new Rectangle2D.Double(vpx, viewportBounds.getY(), viewportBounds.getWidth(),
				viewportBounds.getHeight());
		Rectangle2D vp3 = new Rectangle2D.Double(vpx - sizeInPixels.getWidth(), viewportBounds.getY(),
				viewportBounds.getWidth(), viewportBounds.getHeight());

		if (route != null) {
			for (Edge edge : this.route) {
				Point2D startPoint = vertexToPoint(map, edge.getStartPoint());
				Point2D endPoint = vertexToPoint(map, edge.getEndPoint());
				tryPaintLine(startPoint, endPoint, vp2, map, g);
				tryPaintLine(startPoint, endPoint, vp3, map, g);
			}
		}

		for (Waypoint w : getWaypoints()) {
			Point2D point = map.getTileFactory().geoToPixel(w.getPosition(), map.getZoom());
			tryPaintWaypoint(point, vp2, w, map, g);
			tryPaintWaypoint(point, vp3, w, map, g);
		}
	}

	private Point2D vertexToPoint(JXMapViewer map, Vertex v) {
		return map.getTileFactory().geoToPixel(new GeoPosition(v.getLatitude(), v.getLongitude()), map.getZoom());
	}

	private void tryPaintLine(Point2D startPoint, Point2D endPoint, Rectangle2D viewport, JXMapViewer map, Graphics2D g) {
		if (viewport.contains(startPoint) && viewport.contains(endPoint)) {
			int sx = (int) (startPoint.getX() - viewport.getX());
			int sy = (int) (startPoint.getY() - viewport.getY());
			int ex = (int) (endPoint.getX() - viewport.getX());
			int ey = (int) (endPoint.getY() - viewport.getY());

			g.setStroke(new BasicStroke(5.0f));
			g.setColor(ColorSet.Gold);
			g.drawLine(sx, sy, ex, ey);
		}
	}

	private void tryPaintWaypoint(Point2D point, Rectangle2D viewport, Waypoint w, JXMapViewer map, Graphics2D g) {
		if (viewport.contains(point)) {
			int x = (int) (point.getX() - viewport.getX());
			int y = (int) (point.getY() - viewport.getY());
			g.translate(x, y);
			paintWaypoint(w, map, g);
			g.translate(-x, -y);
		}
	}

	protected void paintWaypoint(final Waypoint w, final JXMapViewer map, final Graphics2D g) {
		renderer.paintWaypoint(g, map, w);
	}

}
