package pl.edu.agh.pdwsm.automobil.simulator.core;

import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.edu.agh.pdwsm.automobil.simulator.SimulationConfig;
import pl.edu.agh.pdwsm.automobil.simulator.SimulatorException;

public class SimulationLauncher {

    private static final Logger log = LoggerFactory.getLogger(SimulationLauncher.class);

    public static SimulationController create(SimulationConfig config) {
        LinkedList<AgentModel> agents = loadAgents(config);
        return new SimulationController(agents, config.getStepPeriod(), config.getCarStopPeriod());
    }

    private static LinkedList<AgentModel> loadAgents(SimulationConfig config) {
        LinkedList<AgentModel> agents = new LinkedList<AgentModel>();

        AgentLauncher agentLoader = new AgentLauncher(config.getPlatformPort(), config.getAddress());
		agentLoader.init();

		if (config.getPopulationSize() > 0) {
			String agentNameCore = "Agent_%d";
			for (int agentNumber = 1; agentNumber <= config.getPopulationSize(); agentNumber++) {
				String agentName = String.format(agentNameCore, agentNumber);
                AgentModel agent = tryLoadAgent(agentLoader, agentName, config.getAgentClassName());
                if (agent != null) {
                    agent.init();
                    agents.add(agent);
				}
			}
		}

		if (agents.size() == 0) {
			String errorMessage = "No agent is taking part in simulation";
			log.error(errorMessage);
			throw new SimulatorException(errorMessage);
		}

		return agents;
	}

	private static AgentModel tryLoadAgent(AgentLauncher agentLoader, String agentName, String className) {
		AgentModel agent = agentLoader.load(agentName, className);
		String logMessage = String.format("Agent %s [type: %s] created", agentName, className);
		log.info(logMessage);

		try {
			return agent;
		} catch (Exception ex) {
			logMessage = String.format("Error while initializing %s agent. It will not take part in simulation",
					agentName);
			log.error(logMessage);
		}

		return null;
	}
}
