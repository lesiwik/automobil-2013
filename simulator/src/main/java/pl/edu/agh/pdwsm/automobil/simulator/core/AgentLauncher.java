package pl.edu.agh.pdwsm.automobil.simulator.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import pl.edu.agh.pdwsm.automobil.simulator.SimulatorException;

class AgentLauncher {
	private static final Logger log = LoggerFactory.getLogger(AgentLauncher.class);

	private AgentContainer platformContainer;
	private final int port;
	private final String address;

	public AgentLauncher(int port, String address) {
		this.port = port;
		this.address = address;
	}

	public void init() {
		try {
			ProfileImpl containerProfile = new ProfileImpl(address, port, null);
			this.platformContainer = Runtime.instance().createAgentContainer(containerProfile);
		} catch (Exception ex) {
			String logMessage = String.format(
					"Error while instantiating JADE environment. Ensure that the port %d is available.", this.port);
			throw new SimulatorException(logMessage, ex);
		}

		if (this.platformContainer == null) {
			String logMessage = String
					.format("Error while connecting to JADE environment. Ensure that JADE platform is available at the port %d."
							+ " Otherwise run the server issuing command \"mvn exec:java\" in \"../automobil-2013/server directory\"",
							this.port);
			log.error(logMessage);
			throw new SimulatorException(logMessage);
		}
	}

	public AgentModel load(String agentName, String agentClassName) {
		try {
			AgentController agentController = platformContainer.createNewAgent(agentName, agentClassName, null);
			return new AgentModel(agentName, agentController);
		} catch (Exception ex) {
			String logMessage = String.format("Error while creating agent %s.", agentName);
			throw new SimulatorException(logMessage, ex);
		}
	}
}
