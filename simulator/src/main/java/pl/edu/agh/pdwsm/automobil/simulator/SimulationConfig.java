package pl.edu.agh.pdwsm.automobil.simulator;

import org.joda.time.Duration;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Required;

public class SimulationConfig {
    private String address;
	private int platformPort;
	private int populationSize;
	private String agentClassName;
    private Duration carStopPeriod;
	private Period stepPeriod;

    public Duration getCarStopPeriod() {
        return carStopPeriod;
    }

    @Required
    public void setCarStopPeriod(Duration carStopPeriod) {
        this.carStopPeriod = carStopPeriod;
    }

    @Required
	public void setPlatformPort(int platformPort) {
		this.platformPort = platformPort;
	}

	public int getPlatformPort() {
		return platformPort;
	}

    public int getPopulationSize() {
		return populationSize;
	}

    @Required
	public void setPopulationSize(int populationSize) {
		this.populationSize = populationSize;
	}

	public String getAgentClassName() {
		return agentClassName;
	}

    @Required
	public void setAgentClassName(String agentClassName) {
		this.agentClassName = agentClassName;
	}

    public String getAddress() {
        return address;
    }

    @Required
    public void setAddress(String address) {
        this.address = address;
    }
    
	public Period getStepPeriod() {
		return stepPeriod;
	}

    @Required
	public void setStepPeriod(Period stepPeriod) {
		this.stepPeriod = stepPeriod;
	}
}
