package pl.edu.agh.pdwsm.automobil.simulator.gui;

import org.jdesktop.swingx.mapviewer.Waypoint;

import pl.edu.agh.pdwsm.automobil.ontology.Position;
import pl.edu.agh.pdwsm.automobil.simulator.core.AgentModel;

public class AgentWaypoint extends Waypoint {
	private final boolean isSelected;

	public static AgentWaypoint create(AgentModel agent) {
		Position position = agent.currentPosition();
		double longitude = position.getLongitude();
		double latitude = position.getLatitude();
		return new AgentWaypoint(longitude, latitude, agent.isSelected());
	}

	private AgentWaypoint(double longitude, double latitude, boolean isSelected) {
		super(latitude, longitude);
		this.isSelected = isSelected;
	}

	public boolean isSelected() {
		return isSelected;
	}
}
