package pl.edu.agh.pdwsm.automobil.simulator.gui;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapKit.DefaultProviders;
import org.jdesktop.swingx.mapviewer.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import pl.edu.agh.pdwsm.automobil.simulator.core.AgentModel;
import pl.edu.agh.pdwsm.automobil.simulator.core.AgentSimulationTask;
import pl.edu.agh.pdwsm.automobil.simulator.core.SimulationController;
import pl.edu.agh.pdwsm.automobil.simulator.core.TimeUpdaterTask;
import pl.edu.agh.pdwsm.automobil.simulator.demo.CoordinationHelper;
import pl.edu.agh.pdwsm.automobil.simulator.gui.RouteScheduler.LocationModel;
import pl.edu.agh.pdwsm.automobil.simulator.gui.icons.ColorSet;
import pl.edu.agh.pdwsm.automobil.simulator.gui.icons.IconSet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MainAppFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    private int delay = 100;
    private final SimulationController simulationController;
    private final RouteScheduler routeScheduler;
    private final AgentPainter agentPainter;

    private JPopupMenu popupMenu;
    private JLabel currentStepLabel;
    private JLabel currentTimeLabel;
    private DateTimeFormatter timeFormatter;
    private JXMapKit mapKit;
    private JButton playButton;
    private JButton pauseButton;
    private JCheckBox isContiniousCheckbox;
    private JSpinner delaySpinner;

    private JLabel toLabel;
    private JLabel fromLabel;

    protected List<AgentSimulationTask> tasks = new ArrayList<AgentSimulationTask>();
    private TimeUpdaterTask timeUpdateTask;

    public MainAppFrame(SimulationController simulationController) {
        this.simulationController = simulationController;
        this.routeScheduler = new RouteScheduler();
        this.agentPainter = new AgentPainter();

        createGUI();
    }

    private void createGUI() {
        this.setLayout(new BorderLayout());

        this.timeFormatter = DateTimeFormat.mediumTime();
        this.currentTimeLabel = new JLabel();
        this.currentTimeLabel.setPreferredSize(new Dimension(80, 22));

        this.currentStepLabel = new JLabel();
        this.currentStepLabel.setPreferredSize(new Dimension(80, 22));

        this.playButton = new JButton(IconSet.Go);
        this.pauseButton = new JButton(IconSet.Pause);
        JButton goAllButton = new JButton("Run them all");
        this.mapKit = createMapKit();
        this.isContiniousCheckbox = new JCheckBox("Continuous simulation", true);
        this.delaySpinner = createDelaySpinner();

        JPanel timePanel = new JPanel(new FlowLayout());
        JLabel timeTitleLabel = new JLabel("Time:", IconSet.Alarm, JLabel.LEFT);
        timePanel.setBackground(ColorSet.VeryLightGray);
        timePanel.add(timeTitleLabel);
        timePanel.add(currentTimeLabel);

        JPanel settingsPanel = new JPanel(new BorderLayout());
        settingsPanel.add(new JLabel("Delay"), BorderLayout.LINE_START);
        settingsPanel.add(delaySpinner, BorderLayout.CENTER);
        settingsPanel.add(isContiniousCheckbox, BorderLayout.PAGE_END);

        JPanel stepPanel = new JPanel(new FlowLayout());
        stepPanel.setBackground(ColorSet.VeryLightGray);
        JLabel stepTitleLabel = new JLabel("Step:", IconSet.Score, JLabel.LEFT);
        stepPanel.add(stepTitleLabel);
        stepPanel.add(currentStepLabel);
        stepPanel.add(settingsPanel);
        stepPanel.add(playButton);
        stepPanel.add(pauseButton);
        stepPanel.setBackground(ColorSet.VeryLightGray);
        pauseButton.setEnabled(false);

        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bottomPanel.setBackground(ColorSet.VeryLightGray);
        bottomPanel.add(timePanel);
        bottomPanel.add(stepPanel);

        JPanel listPanel = new JPanel();
        listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.PAGE_AXIS));
        listPanel.setBackground(ColorSet.VeryLightGray);
        final JList<AgentModel> agentsList = new JList<AgentModel>();
        agentsList.setBackground(ColorSet.VeryLightGray);
        final DefaultListModel<AgentModel> agentModel = this.simulationController.getAgentModel();
        agentsList.setModel(agentModel);

        this.popupMenu = new JPopupMenu();
        JMenuItem menuItem = new JMenuItem("Go");
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg) {
                Optional<AgentModel> agentModel = Iterators.tryFind(simulationController.getAgents().iterator(),
                        new Predicate<AgentModel>() {
                            @Override
                            public boolean apply(AgentModel agent) {
                                return agent.isSelected();
                            }
                        });
                if (agentModel.isPresent()) {
                    LocationModel from = routeScheduler.getFrom();
                    LocationModel to = routeScheduler.getTo();
                    if (!from.equals(RouteScheduler.DefaultLocation) && !to.equals(RouteScheduler.DefaultLocation)) {
                        simulationController.registerRoute(agentModel.get(), from.getPositon(), to.getPositon());
                    }
                }
            }
        });
        this.popupMenu.add(menuItem);
        JMenuItem privilegedItem = new JMenuItem("Toggle privileged");
        privilegedItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Optional<AgentModel> agentModel = Iterators.tryFind(simulationController.getAgents().iterator(),
                        new Predicate<AgentModel>() {
                            @Override
                            public boolean apply(AgentModel agent) {
                                return agent.isSelected();
                            }
                        });
                if (agentModel.isPresent()) {
                    agentModel.get().setPrivileged(!agentModel.get().isPrivileged());
                }
            }
        });
        this.popupMenu.add(privilegedItem);

        agentsList.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                check(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                check(e);
            }

            public void check(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    agentsList.setSelectedIndex(agentsList.locationToIndex(e.getPoint()));
                    popupMenu.show(agentsList, e.getX(), e.getY());
                }
            }
        });
        agentsList.setCellRenderer(new AgentCellRenderer());
        agentsList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel lsm = (ListSelectionModel) e.getSource();
                for (int elementIndex = 0; elementIndex < agentModel.size(); elementIndex++) {
                    AgentModel agent = agentModel.get(elementIndex);
                    agent.setSelected(lsm.isSelectedIndex(elementIndex));
                }

                updateGui();
            }
        });

        JScrollPane listScroller = new JScrollPane(agentsList);
        listScroller.setBackground(ColorSet.VeryLightGray);
        listScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        listScroller.setPreferredSize(new Dimension(200, 600));
        listPanel.add(listScroller);

        JPanel goButtonPanel = new JPanel();
        goButtonPanel.add(goAllButton);
        goAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                for (AgentModel model : simulationController.getAgents()) {
                    LocationModel from = routeScheduler.getFrom();
                    LocationModel to = routeScheduler.getTo();
                    if (!from.equals(RouteScheduler.DefaultLocation) && !to.equals(RouteScheduler.DefaultLocation)) {
                        simulationController.registerRoute(model, from.getPositon(), to.getPositon());
                    }
                }
            }
        });
        listPanel.add(goButtonPanel);

        this.fromLabel = new JLabel();
        this.toLabel = new JLabel();
        JPanel twoClickPanel = new JPanel();
        twoClickPanel.setLayout(new GridLayout(2, 2));
        twoClickPanel.add(new JLabel(" From: "));
        twoClickPanel.add(this.fromLabel);
        twoClickPanel.add(new JLabel(" To: "));
        twoClickPanel.add(this.toLabel);
        twoClickPanel.setPreferredSize(new Dimension(200, 40));
        twoClickPanel.setBackground(ColorSet.VeryLightGray);
        listPanel.add(twoClickPanel);

        this.setIconImage(IconSet.Maps.getImage());
        this.setTitle("Automobil - Simulator");

        this.add(mapKit, BorderLayout.CENTER);
        this.add(bottomPanel, BorderLayout.SOUTH);
        this.add(listPanel, BorderLayout.EAST);
        this.pack();

        updateGui();

        this.playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isContiniousCheckbox.isSelected()) {
                    stepAndUpdate();
                } else {
                    playButton.setEnabled(false);
                    pauseButton.setEnabled(true);
                    isContiniousCheckbox.setEnabled(false);
                    delaySpinner.setEnabled(false);
                    runThreads();
                }
            }

            private void runThreads() {
                tasks.clear();
                for (AgentModel agent : simulationController.getAgents()) {
                    AgentSimulationTask task = new AgentSimulationTask(agent, simulationController, getMain(), delay);
                    tasks.add(task);
                    new Thread(task).start();
                }
                timeUpdateTask = new TimeUpdaterTask(simulationController, delay);
                new Thread(timeUpdateTask).start();
            }

            private void stepAndUpdate() {
                for (AgentModel agent : simulationController.getAgents()) {
                    simulationController.nextStep(agent);
                    updateGui();
                }
            }
        });

        this.pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (AgentSimulationTask task : tasks) {
                    task.pause();
                }
                timeUpdateTask.pause();
                playButton.setEnabled(true);
                pauseButton.setEnabled(false);
                isContiniousCheckbox.setEnabled(true);
                delaySpinner.setEnabled(true);
            }
        });

        this.delaySpinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                delay = (Integer) delaySpinner.getValue();
                for (AgentSimulationTask task : tasks) {
                    task.setDelay(delay);
                }
                if (timeUpdateTask != null) {
                    timeUpdateTask.setDelay(delay);
                }
            }
        });
    }

    protected MainAppFrame getMain() {
        return this;
    }

    private JSpinner createDelaySpinner() {
        SpinnerModel model = new SpinnerNumberModel(100, 1, 10000, 1);
        return new JSpinner(model);
    }

    public void updateGui() {
        updateCurrentStateLabel();
        updateCurrentTimeLabel();
        updateCurrentPath();
        updateAgentPositions();
    }

    private void updateCurrentPath() {
        for (AgentModel agent : simulationController.getAgents()) {
            if (agent.isSelected()) {
                agentPainter.setRoute(agent.currentPath());
                return;
            }
        }
        agentPainter.setRoute(null);
    }

    private void updateAgentPositions() {
        HashSet<Waypoint> waypoints = new HashSet<Waypoint>();

        for (AgentModel agent : simulationController.getAgents()) {
            final Waypoint waypoint = AgentWaypoint.create(agent);
            waypoints.add(waypoint);
        }

        this.agentPainter.setWaypoints(waypoints);
        this.mapKit.getMainMap().setOverlayPainter(this.agentPainter);
    }

    private void updateCurrentTimeLabel() {
        DateTime currentTime = simulationController.getTime();
        String currentTimeText = this.timeFormatter.print(currentTime);
        this.currentTimeLabel.setText(currentTimeText);
    }

    private void updateCurrentStateLabel() {
        int currentStep = simulationController.getStepNumber();

        String currentStepText = String.format("%3d", currentStep);
        this.currentStepLabel.setText(currentStepText);
    }

    private JXMapKit createMapKit() {
        final JXMapKit kit = new JXMapKit();
        kit.setDefaultProvider(DefaultProviders.OpenStreetMaps);

        final int max = 17;
        TileFactoryInfo info = new TileFactoryInfo(1, max - 2, max, 256, true, true, "http://tile.openstreetmap.org", "x", "y", "z") {
            @Override
            public String getTileUrl(int x, int y, int zoom) {
                zoom = max - zoom;
                String url = this.baseURL + "/" + zoom + "/" + x + "/" + y + ".png";
                return url;
            }

        };
        TileFactory tf = new DefaultTileFactory(info);
        kit.setTileFactory(tf);
        kit.setZoom(5);
        kit.setAddressLocationShown(false);
        kit.getMainMap().setDrawTileBorders(false);
        kit.getMainMap().setRestrictOutsidePanning(true);
        kit.getMainMap().setHorizontalWrapped(false);
        kit.setCenterPosition(CoordinationHelper.Krakow);
        kit.setMiniMapVisible(false);
        kit.getMainMap().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                final GeoPosition position = kit.getMainMap().convertPointToGeoPosition(e.getPoint());
                routeScheduler.addLocation(position);
                updateDesitinationLabels();
            }
        });

        return kit;
    }

    private void updateDesitinationLabels() {
        fromLabel.setText(routeScheduler.getFrom().toString());
        toLabel.setText(routeScheduler.getTo().toString());
    }

    private static final class AgentCellRenderer implements ListCellRenderer<AgentModel> {

        @Override
        public Component getListCellRendererComponent(JList<? extends AgentModel> list, AgentModel model, int index, boolean isSelected,
                boolean hasFocus) {
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING, 3, 3));
            JLabel label = new JLabel();
            if (model != null) {
                label.setText(model.getName());
            } else {
                label.setText("null");
            }
            panel.add(label);

            Color color = model.isPrivileged() ? ColorSet.Crimson : Color.GRAY;

            if (isSelected) {
                panel.setBackground(ColorSet.addApha(color, ColorSet.HighAlpha));
            } else {
                panel.setBackground(color);
            }

            return panel;
        }
    }
}
