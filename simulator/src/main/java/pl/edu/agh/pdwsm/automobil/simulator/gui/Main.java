package pl.edu.agh.pdwsm.automobil.simulator.gui;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.edu.agh.pdwsm.automobil.simulator.SimulationConfig;
import pl.edu.agh.pdwsm.automobil.simulator.SimulatorException;
import pl.edu.agh.pdwsm.automobil.simulator.core.SimulationController;
import pl.edu.agh.pdwsm.automobil.simulator.core.SimulationLauncher;

public class Main {
	private static final String SpringConfigurationFile = "classpath:spring/spring-beans.xml";

	private static final Logger log = LoggerFactory.getLogger(Main.class);

	private static void createAndShowGUI(SimulationController simulationController) {
		JFrame frame = new MainAppFrame(simulationController);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1200, 800);
		frame.setVisible(true);
	}

	private static SimulationController createSimulationController() {
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/spring-beans.xml");
			try {
				SimulationConfig simulationConfig = context.getBean(SimulationConfig.class);
				return SimulationLauncher.create(simulationConfig);
			} catch (BeansException ex) {
				String logMessage = String.format("Error while resolving configuration of the simulation [type: %s]."
						+ " Ensure that the bean is defined in the configuration.", SimulationConfig.class.getName());
				log.error(logMessage, ex);
				throw new SimulatorException(ex);
			}
		} catch (BeansException ex) {
			String errorMessage = "Error looking up Spring configuration. Ensure that the Spring configuration conforming naming convention "
					+ SpringConfigurationFile + " is accessible from classpath.";

			log.error(errorMessage, ex);
			throw new SimulatorException(ex);
		}
	}

	public static void main(String[] args) {
		final SimulationController simulationController = createSimulationController();

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI(simulationController);
			}
		});
	}
}
