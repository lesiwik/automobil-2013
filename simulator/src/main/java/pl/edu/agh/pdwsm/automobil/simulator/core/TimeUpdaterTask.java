package pl.edu.agh.pdwsm.automobil.simulator.core;

import java.util.concurrent.atomic.AtomicBoolean;

public class TimeUpdaterTask implements Runnable {

    private volatile AtomicBoolean continiueThread = new AtomicBoolean();
    private final SimulationController simulationController;
    private long delay;

    public TimeUpdaterTask(SimulationController simulationController, long delay) {
        this.simulationController = simulationController;
        this.delay = delay;
    }

    @Override
    public void run() {
        continiueThread.set(true);
        while (continiueThread.get()) {
            try {
                simulationController.updateTime();
                Thread.sleep(delay);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void pause() {
        continiueThread.set(false);
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

}
