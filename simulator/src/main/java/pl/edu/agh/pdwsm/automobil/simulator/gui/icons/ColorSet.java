package pl.edu.agh.pdwsm.automobil.simulator.gui.icons;

import java.awt.Color;

public class ColorSet {

	public static final int HighAlpha = 80;
	public static final int MediumAlpha = 50;
	public static final int LowAlpha = 30;

	public static final Color DarkGray = new Color(169, 169, 169);
	public static final Color DimGray = new Color(105, 105, 105);
	public static final Color Gray = new Color(80, 80, 80);
	public static final Color LightGray = new Color(211, 211, 211);
	public static final Color VeryLightGray = new Color(225, 225, 225);
	public static final Color Crimson = new Color(220, 20, 60);
	public static final Color Gold = new Color(255, 215, 0);

	public static Color addApha(Color color, int alpha) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha);
	}
}
