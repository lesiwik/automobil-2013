package pl.edu.agh.pdwsm.automobil.simulator.core;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.pdwsm.automobil.agent.MobileAgent;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.ontology.Position;

import java.util.List;
import java.util.Random;

import static com.google.common.collect.Iterables.tryFind;

public class AgentModel implements MobileAgent {
    private static final Logger log = LoggerFactory.getLogger(AgentModel.class);
    private static final int DEFAULT_SPEED = 14; // ~50km/h

    private final String name;
    private final AgentController agent;
    private boolean isSelected;
    private MobileAgent agentProxy;
    private double speed;
    private DateTime arrivalTime;
    private boolean isPrivileged = false;
    private Random rand = new Random();

    public AgentModel(String name, AgentController agent) {
        this.name = name;
        this.agent = agent;
        this.isSelected = false;
    }

    public boolean isInactiveForTooLong(DateTime currentTime, Duration carStopPeriod) {
        if (arrivalTime == null) {
            arrivalTime = currentTime;
            return true;
        }
        Duration inactivityTime = new Duration(currentTime.getMillis(), arrivalTime.getMillis());
        return inactivityTime.isLongerThan(carStopPeriod);
    }

    public void init() {
        try {
            agentProxy = agent.getO2AInterface(MobileAgent.class);
            if (agentProxy != null) {

                String logMessage = String.format("Agent %s was initialized", name);
                log.info(logMessage);
            } else {
                String logMessage = String.format("Agent %s has not registered proxy %s. It will not take part in simulation.", name,
                        MobileAgent.class.getName());
                log.error(logMessage);
            }
        } catch (Exception ex) {
            String logMessage = String.format("Error while calling [init] on agent %s. The agent will be ignored in simulation.", name);
            log.error(logMessage, ex);
        }
    }

    private boolean isInitialized() {
        return agentProxy != null;
    }

    @Override
    public void updatePosition(Position position) {
        if (isInitialized()) {
            try {
                agentProxy.updatePosition(position);
            } catch (Exception ex) {
                String logMessage = String.format("Error while calling [updatePosition] on agent %s.", name);
                log.error(logMessage, ex);
            }
        }
    }

    @Override
    public void startRouting(Position startPoint, Position endPoint, boolean priviliged) {
        try {
            agent.start();
            agentProxy.startRouting(startPoint, endPoint, priviliged);
            arrivalTime = null;
        } catch (StaleProxyException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Edge> currentPath() {
        return agentProxy.currentPath();
    }

    @Override
    public Position currentPosition() {
        return agentProxy.currentPosition();
    }

    @Override
    public boolean isNavigating() {
        return agentProxy.isNavigating();
    }

    double getSpeed() {
        List<Edge> path = agentProxy.currentPath();
        Optional<Edge> edgeOptional = tryFind(path, new Predicate<Edge>() {
            @Override
            public boolean apply(Edge input) {
                return input.getLength() > 20;
            }
        });
        if (edgeOptional.isPresent()) {
            int perfectSpeed = edgeOptional.get().getPerfectSpeed();
            if (perfectSpeed == 0) {
                perfectSpeed = DEFAULT_SPEED;
            }

            if(isPrivileged){
                speed = perfectSpeed + 2;
            }

            if (speed < perfectSpeed) {
                speed += rand.nextInt(5) - 2;
            }
            if (speed > perfectSpeed) {
                speed = perfectSpeed;
            } else if (speed < perfectSpeed / 2) {
                speed = perfectSpeed / 2;
            }
        }
        if (speed == 0) {
            speed = DEFAULT_SPEED;
        }
        return speed;
    }

    public String getName() {
        return this.name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isPrivileged() {
        return isPrivileged;
    }

    public void setPrivileged(boolean isPriviliged) {
        this.isPrivileged = isPriviliged;
    }
}
