package pl.edu.agh.pdwsm.automobil.simulator.core;

import java.util.concurrent.atomic.AtomicBoolean;

import pl.edu.agh.pdwsm.automobil.simulator.gui.MainAppFrame;

public class AgentSimulationTask implements Runnable {

    private final AgentModel agentModel;
    private volatile AtomicBoolean continiueThread = new AtomicBoolean();
    private final SimulationController simulationController;
    private final MainAppFrame mainFrame;
    private long delay;

    public AgentSimulationTask(AgentModel agentModel, SimulationController simulationController, MainAppFrame mainFrame, long delay) {
        super();
        this.agentModel = agentModel;
        this.simulationController = simulationController;
        this.mainFrame = mainFrame;
        this.delay = delay;
    }

    @Override
    public void run() {
        continiueThread.set(true);
        while (continiueThread.get()) {
            try {
                stepAndUpdate();
                Thread.sleep(delay);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void stepAndUpdate() {
        simulationController.nextStep(agentModel);
        mainFrame.updateGui();
    }

    public void pause() {
        continiueThread.set(false);
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

}
