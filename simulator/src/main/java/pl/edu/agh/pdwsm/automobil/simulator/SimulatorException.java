package pl.edu.agh.pdwsm.automobil.simulator;

public class SimulatorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SimulatorException(String msg, Exception ex) {
		super(msg, ex);
	}

	public SimulatorException(String msg) {
		super(msg);
	}

	public SimulatorException(Exception ex) {
		super(ex);
	}

}
