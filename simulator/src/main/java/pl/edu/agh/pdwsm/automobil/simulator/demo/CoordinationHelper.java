package pl.edu.agh.pdwsm.automobil.simulator.demo;

import java.util.Random;

import org.jdesktop.swingx.mapviewer.GeoPosition;

public class CoordinationHelper {
	private static final double ParisLatitudeBegin = 48.80;
	private static final double ParisLatitudeEnd = 48.9;
	private static final double ParisLongitudeEnd = 2.40;
	private static final double ParisLongitudeBegin = 2.20;

	private final Random random = new Random();

	public static final GeoPosition Paris = new GeoPosition(48.84, 2.30);
    public static final GeoPosition Krakow = new GeoPosition(50.06, 19.93);

	public GeoPosition getRandomPositionInParis() {
		return new GeoPosition(getRandomParisLatitude(), getRandomParisLongitude());
	}

	private double getRandomParisLongitude() {
		return ParisLongitudeBegin + random.nextGaussian() * (ParisLongitudeEnd - ParisLongitudeBegin);
	}

	private double getRandomParisLatitude() {
		return ParisLatitudeBegin + random.nextGaussian() * (ParisLatitudeEnd - ParisLatitudeBegin);
	}
}
