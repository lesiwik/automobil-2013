package pl.edu.agh.pdwsm.automobil.simulator.gui;

import java.util.LinkedList;

import org.jdesktop.swingx.mapviewer.GeoPosition;

import com.google.common.collect.Lists;
import com.google.common.hash.Hashing;

import pl.edu.agh.pdwsm.automobil.ontology.Position;

public class RouteScheduler {

	public static final LocationModel DefaultLocation = LocationModel.create(new GeoPosition(0.0, 0.0));

	private LinkedList<LocationModel> locationQueue;

	public RouteScheduler() {
		this.locationQueue = Lists.newLinkedList();
	}

	public void addLocation(GeoPosition position) {
		final LocationModel locationModel = LocationModel.create(position);

		locationQueue.add(locationModel);
		while (locationQueue.size() > 2) {
			locationQueue.remove();
		}
	}

	public LocationModel getFrom() {
		if (locationQueue.size() > 1) {
			return locationQueue.get(1);
		}
		return DefaultLocation;
	}

	public LocationModel getTo() {
		if (locationQueue.size() > 0) {
			return locationQueue.get(0);
		}
		return DefaultLocation;
	}

	public static final class LocationModel {
		private final Position position;

		public static LocationModel create(GeoPosition geoPosition) {
			Position position = new Position();
			position.setLatitude(geoPosition.getLatitude());
			position.setLongitude(geoPosition.getLongitude());
			return new LocationModel(position);
		}

		private LocationModel(Position position) {
			this.position = position;
		}

		public Position getPositon() {
			return this.position;
		}

		@Override
		public String toString() {
			return String.format("%2.2f,%2.2f", position.getLongitude(), position.getLatitude());
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof LocationModel) {
				LocationModel castedObj = (LocationModel) obj;

				if (this.position == null) {
					return (castedObj.position != null);
				}

				return this.position.equals(castedObj.getPositon());
			}
			return false;
		}

		@Override
		public int hashCode() {
			return Hashing.md5().newHasher().putDouble(position.getLongitude()).putDouble(position.getLatitude())
					.hash().hashCode();
		}
	}
}
