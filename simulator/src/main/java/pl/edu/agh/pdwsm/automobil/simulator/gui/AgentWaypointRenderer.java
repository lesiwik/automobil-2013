package pl.edu.agh.pdwsm.automobil.simulator.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;

import pl.edu.agh.pdwsm.automobil.simulator.gui.icons.ColorSet;

public class AgentWaypointRenderer implements WaypointRenderer {

	public AgentWaypointRenderer() {
	}

	public boolean paintWaypoint(Graphics2D g, JXMapViewer map, Waypoint waypoint) {
		g.setStroke(new BasicStroke(1.5f));
		g.setColor(Color.BLUE);

		if (waypoint instanceof AgentWaypoint) {
			AgentWaypoint agentWaypoint = (AgentWaypoint) waypoint;
			if (agentWaypoint.isSelected()) {
				g.setStroke(new BasicStroke(3f));
				g.setColor(ColorSet.Crimson);
			}
		}

		g.drawOval(-10, -10, 20, 20);
		g.setStroke(new BasicStroke(1f));
		g.drawLine(-10, 0, 10, 0);
		g.drawLine(0, -10, 0, 10);
		return false;
	}
}
