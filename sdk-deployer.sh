#!/bin/bash

mvn install:install-file -Dfile=libs/jade.jar -DgroupId=net.sf.ingenias -DartifactId=jade -Dversion=4.3 -Dpackaging=jar
mvn install:install-file -Dfile=libs/JadeAndroid.jar -DgroupId=net.sf.ingenias -DartifactId=jade-android -Dversion=4.3 -Dpackaging=jar

DIR=/tmp/maven-android-sdk-deployer
git clone https://github.com/mosabua/maven-android-sdk-deployer.git $DIR
cd $DIR
mvn install -P 4.2
cd extras/google-play-services
mvn install
