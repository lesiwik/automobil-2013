package pl.edu.agh.pdwsm.automobil.jade;

import jade.core.AID;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;

public class RequestInitiatorImplTest {
	private static final int TEST_PORT = 9999;
	private static final int SHORT_DELAY = 100;
	private static final String AGENT_NAME = "agent";
	private static final String DUMMY_QUERY = "test ping";
	private static final String DUMMY_RESULT = "test pong";
	private static final String PROTOCOL = "protocol";

	private static PlatformContainer platformContainer;

	private AgentId agentId;
	private AgentController agent;
	private CommunicationService communicationProvider;

	@Before
	public void setUp() throws ContainerException, StaleProxyException {
		platformContainer = PlatformContainer.create(TEST_PORT);

		agentId = new AgentId(new AID(AGENT_NAME, AID.ISLOCALNAME));
		agent = platformContainer.createAgent(AGENT_NAME, CommunicationAgent.class, new Object[0]);
		agent.start();

		communicationProvider = agent.getO2AInterface(CommunicationService.class);
	}

	@After
	public void tearDown() throws StaleProxyException, InterruptedException, ContainerException {
		agent.kill();
		platformContainer.kill();
	}

	@Test
	public void testInvokeResponseHandlerOnValidResponse() {
		try {
			CounterResponseHandler handler = new CounterResponseHandler();
			communicationProvider.startQueryService(PROTOCOL, new DummyRequestHandler(DUMMY_RESULT));
			shortDelay();
			communicationProvider.query(agentId, PROTOCOL, null, DUMMY_QUERY, handler);
			shortDelay();

			Assert.assertEquals(1, handler.getSuccessCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testInvokeResponseRefuseHandlerOnTimeoutWhenQueryServiceNotAvailable() {
		try {
			CounterResponseHandler handler = new CounterResponseHandler();
			communicationProvider.query(agentId, PROTOCOL, Long.valueOf(10), DUMMY_QUERY, handler);
			shortDelay();

			Assert.assertEquals(1, handler.getRefuseCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testInvokeResponseFailureHandlerOnNullResponse() {
		try {
			CounterResponseHandler handler = new CounterResponseHandler();
			communicationProvider.startQueryService(PROTOCOL, new DummyRequestHandler(null));
			shortDelay();
			communicationProvider.query(agentId, PROTOCOL, null, DUMMY_QUERY, handler);
			shortDelay();

			Assert.assertEquals(1, handler.getFailureCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	private static void shortDelay() {
		try {
			Thread.sleep(SHORT_DELAY);
		} catch (InterruptedException ex) {
		}
	}

	private static class CounterResponseHandler implements ResponseHandler {

		private int successCount = 0;
		private int failureCount = 0;
		private int refuseCount = 0;

		@Override
		public void handleSuccess(Serializable response) {
			successCount++;
		}

		@Override
		public void handleFailure() {
			failureCount++;
		}

		@Override
		public void handleRefuse() {
			refuseCount++;
		}

		public int getSuccessCount() {
			return successCount;
		}

		public int getFailureCount() {
			return failureCount;
		}

		public int getRefuseCount() {
			return refuseCount;
		}
	}

	private static class DummyRequestHandler implements RequestHandler {
		private final String result;
		
		public DummyRequestHandler(String result) {
			this.result = result;
		}
		
		@Override
		public Serializable handle(Serializable request) {
			return result;
		}
	}
}
