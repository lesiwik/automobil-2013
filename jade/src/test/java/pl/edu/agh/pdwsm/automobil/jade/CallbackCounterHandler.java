package pl.edu.agh.pdwsm.automobil.jade;

public class CallbackCounterHandler<T> implements NotificationHandler<T> {

	private int callCount = 0;

	@Override
	public void OnMessage(T body) {
		++callCount;
	}

	public int getCallCount() {
		return callCount;
	}
}
