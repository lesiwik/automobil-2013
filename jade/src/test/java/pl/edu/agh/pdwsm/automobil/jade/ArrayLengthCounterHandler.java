package pl.edu.agh.pdwsm.automobil.jade;

public class ArrayLengthCounterHandler<T> implements NotificationHandler<T[]>{

	private int allObjectCount;
	
	@Override
	public void OnMessage(T[] body) {
		if(body != null) {
			allObjectCount += body.length;
		}
	}

	public int getAllObjectCount() {
		return allObjectCount;
	}
}
