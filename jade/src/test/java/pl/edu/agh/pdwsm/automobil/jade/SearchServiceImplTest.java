package pl.edu.agh.pdwsm.automobil.jade;

import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SearchServiceImplTest {
	private static final int TEST_PORT = 9999;
	private static final int SHORT_DELAY = 20;
	private static final String AGENT_NAME = "agent";
	private static final String SERVICE_NAME = "service";
	private static final String SERVICE_TYPE = "dummyService";
	private static final String FAKE_SERVICE_NAME = "fakeService";

	private static PlatformContainer platformContainer;

	private AgentController agent;
	private CommunicationService communicationProvider;

	@Before
	public void setUp() throws ContainerException, StaleProxyException {
		platformContainer = PlatformContainer.create(TEST_PORT);
		agent = platformContainer.createAgent(AGENT_NAME, CommunicationAgent.class, new Object[0]);
		agent.start();

		communicationProvider = agent.getO2AInterface(CommunicationService.class);
	}

	@After
	public void tearDown() throws StaleProxyException, InterruptedException, ContainerException {
		agent.kill();
		platformContainer.kill();
	}

	@Test
	public void testInvokeHandlerOnSearchedService() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			communicationProvider.searchService(SERVICE_NAME, SERVICE_TYPE, null, handler);
			shortDelay();

			Assert.assertEquals(1, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testInvokeHandlerOnSearchedServiceWhenServiceNameIsUnknown() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			communicationProvider.searchService(null, SERVICE_TYPE, null, handler);
			shortDelay();

			Assert.assertEquals(1, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testInvokeHandlerOnSearchedServiceServiceTypeIsUnknown() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			communicationProvider.searchService(SERVICE_NAME, null, null, handler);
			shortDelay();

			Assert.assertEquals(1, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testDoNotInvokeHandlerOnOtherService() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			communicationProvider.searchService(FAKE_SERVICE_NAME, null, null, handler);
			shortDelay();

			Assert.assertEquals(0, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testDoNotInvokeHandlerWhenServiceWasDeregistered() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			CancelAware token = communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			token.cancel();
			shortDelay();
			communicationProvider.searchService(SERVICE_NAME, SERVICE_TYPE, null, handler);
			shortDelay();

			Assert.assertTrue(token.isCancelled());
			Assert.assertEquals(0, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	private static void shortDelay() {
		try {
			Thread.sleep(SHORT_DELAY);
		} catch (InterruptedException ex) {
		}
	}
}
