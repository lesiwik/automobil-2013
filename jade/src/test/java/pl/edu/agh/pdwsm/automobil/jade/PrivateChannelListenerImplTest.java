package pl.edu.agh.pdwsm.automobil.jade;

import jade.core.AID;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PrivateChannelListenerImplTest {

	private static final int TEST_PORT = 9999;
	private static final int SHORT_DELAY = 100;
	private static final String AGENT_NAME = "agent";
	private static final String CHANNEL_NAME = "channel";
	private static final String FAKE_CHANNEL_NAME = "fakechannel";

	private static PlatformContainer platformContainer;

	private AgentId agentId;
	private AgentController agent;
	private CommunicationService communicationProvider;

	@Before
	public void setUp() throws ContainerException, StaleProxyException {
		platformContainer = PlatformContainer.create(TEST_PORT);

		agentId = new AgentId(new AID(AGENT_NAME, AID.ISLOCALNAME));
		agent = platformContainer.createAgent(AGENT_NAME, CommunicationAgent.class, new Object[0]);
		agent.start();

		communicationProvider = agent.getO2AInterface(CommunicationService.class);
	}

	@After
	public void tearDown() throws StaleProxyException, InterruptedException, ContainerException {
		agent.kill();
		platformContainer.kill();
	}

	@Test
	public void testInvokeHandlerOnSubscribedChannel() {
		CallbackCounterHandler<Object> handler = new CallbackCounterHandler<Object>();
		try {
			communicationProvider.openPrivateChannel(CHANNEL_NAME, handler);
			shortDelay();
			communicationProvider.sendPrivateMessage(agentId, CHANNEL_NAME, null);
			shortDelay();

			Assert.assertEquals(1, handler.getCallCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testDoNotInvokeHandlerOnOtherChannel() {
		CallbackCounterHandler<Object> handler = new CallbackCounterHandler<Object>();
		try {
			communicationProvider.openPrivateChannel(CHANNEL_NAME, handler);
			shortDelay();
			communicationProvider.sendPrivateMessage(agentId, FAKE_CHANNEL_NAME, null);
			shortDelay();

			Assert.assertEquals(0, handler.getCallCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testDoNotInvokeHandlerWhenSubscriptionCancelled() {
		CallbackCounterHandler<Object> handler = new CallbackCounterHandler<Object>();
		try {
			CancelAware token = communicationProvider.openPrivateChannel(CHANNEL_NAME, handler);
			shortDelay();
			communicationProvider.sendPrivateMessage(agentId, CHANNEL_NAME, null);
			shortDelay();
			token.cancel();
			shortDelay();
			communicationProvider.sendPrivateMessage(agentId, CHANNEL_NAME, null);

			Assert.assertTrue(token.isCancelled());
			Assert.assertEquals(1, handler.getCallCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}

	private static void shortDelay() {
		try {
			Thread.sleep(SHORT_DELAY);
		} catch (InterruptedException ex) {
		}
	}
}
