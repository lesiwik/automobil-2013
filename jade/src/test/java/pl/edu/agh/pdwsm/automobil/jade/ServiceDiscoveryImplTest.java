package pl.edu.agh.pdwsm.automobil.jade;

import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ServiceDiscoveryImplTest {
	private static final int TEST_PORT = 9999;
	private static final int SHORT_DELAY = 20;
	private static final String AGENT_NAME = "agent";
	private static final String SERVICE_NAME = "service";
	private static final String SERVICE_TYPE = "serviceType";
	private static final String FAKE_SERVICE_NAME = "fakeService";

	private static PlatformContainer platformContainer;

	private AgentController agent;
	private CommunicationService communicationProvider;

	@Before
	public void setUp() throws ContainerException, StaleProxyException {
		platformContainer = PlatformContainer.create(TEST_PORT);
		agent = platformContainer.createAgent(AGENT_NAME, CommunicationAgent.class, new Object[0]);
		agent.start();

		communicationProvider = agent.getO2AInterface(CommunicationService.class);
	}

	@After
	public void tearDown() throws StaleProxyException, InterruptedException, ContainerException {
		agent.kill();
		platformContainer.kill();
	}

	@Test
	public void testInvokeHandlerOnAlreadyRegisteredSearchedService() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			communicationProvider.startServiceDiscovery(SERVICE_NAME, SERVICE_TYPE, null, handler);
			shortDelay();

			Assert.assertEquals(1, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testInvokeHandlerWhenNewServiceIsRegistered() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			communicationProvider.startServiceDiscovery(SERVICE_NAME, SERVICE_TYPE, null, handler);
			shortDelay();
			communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();

			Assert.assertEquals(1, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testInvokeHandlerWhenServiceIsDeregistered() throws InterruptedException {
		CallbackCounterHandler<AgentId[]> handler = new CallbackCounterHandler<AgentId[]>();
		try {
			CancelAware token = communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			communicationProvider.startServiceDiscovery(SERVICE_NAME, SERVICE_TYPE, null, handler);
			shortDelay();
			token.cancel();
			shortDelay();

			Assert.assertTrue(token.isCancelled());
			Assert.assertEquals(2, handler.getCallCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testDoNotInvokeHandlerOnOtherService() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			communicationProvider.startServiceDiscovery(FAKE_SERVICE_NAME, SERVICE_TYPE, null, handler);
			shortDelay();

			Assert.assertEquals(0, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testDoNotInvokeHandlerWhenServiceDiscoveryWasStopped() throws InterruptedException {
		ArrayLengthCounterHandler<AgentId> handler = new ArrayLengthCounterHandler<AgentId>();
		try {
			CancelAware registrationToken = communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();
			
			CancelAware discoveryToken = communicationProvider.startServiceDiscovery(null, SERVICE_TYPE, null, handler);
			shortDelay();
			discoveryToken.cancel();
			shortDelay();
			
			registrationToken.cancel();
			shortDelay();
			communicationProvider.registerService(SERVICE_NAME, SERVICE_TYPE, null);
			shortDelay();

			Assert.assertTrue(registrationToken.isCancelled());
			Assert.assertTrue(discoveryToken.isCancelled());
			Assert.assertEquals(1, handler.getAllObjectCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	private static void shortDelay() {
		try {
			Thread.sleep(SHORT_DELAY);
		} catch (InterruptedException ex) {
		}
	}
}
