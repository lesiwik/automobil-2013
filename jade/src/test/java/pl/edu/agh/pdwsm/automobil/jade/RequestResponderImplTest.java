package pl.edu.agh.pdwsm.automobil.jade;

import jade.core.AID;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;

public class RequestResponderImplTest {
	private static final int TEST_PORT = 9999;
	private static final int SHORT_DELAY = 100;
	private static final String AGENT_NAME = "agent";
	private static final String PROTOCOL = "protocol";

	private static PlatformContainer platformContainer;

	private AgentId agentId;
	private AgentController agent;
	private CommunicationService communicationProvider;

	@Before
	public void setUp() throws ContainerException, StaleProxyException {
		platformContainer = PlatformContainer.create(TEST_PORT);

		agentId = new AgentId(new AID(AGENT_NAME, AID.ISLOCALNAME));
		agent = platformContainer.createAgent(AGENT_NAME, CommunicationAgent.class, new Object[0]);
		agent.start();

		communicationProvider = agent.getO2AInterface(CommunicationService.class);
	}

	@After
	public void tearDown() throws StaleProxyException, InterruptedException, ContainerException {
		agent.kill();
		platformContainer.kill();
	}

	@Test
	public void testInvokeRequestHandlerOnValidRequest() {
		try {
			TestRequestHandler handler = new TestRequestHandler();
			communicationProvider.startQueryService(PROTOCOL, handler);
			shortDelay();
			communicationProvider.query(agentId, PROTOCOL, null, null, new DummyResponseHandler());
			shortDelay();

			Assert.assertEquals(1, handler.getCallCount());
		} catch (Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}

	private static void shortDelay() {
		try {
			Thread.sleep(SHORT_DELAY);
		} catch (InterruptedException ex) {
		}
	}

	private static class DummyResponseHandler implements ResponseHandler {

		@Override
		public void handleSuccess(Serializable response) {
		}

		@Override
		public void handleFailure() {
		}

		@Override
		public void handleRefuse() {
		}

	}

	private static class TestRequestHandler implements RequestHandler {
		private int callCount = 0;

		@Override
		public Serializable handle(Serializable request) {
			callCount++;
			return null;
		}

		public int getCallCount() {
			return callCount;
		}
	}
}
