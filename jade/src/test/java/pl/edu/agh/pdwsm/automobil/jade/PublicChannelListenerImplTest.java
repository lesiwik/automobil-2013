package pl.edu.agh.pdwsm.automobil.jade;

import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PublicChannelListenerImplTest {

	private static final int TEST_PORT = 9999;
	private static final int SHORT_DELAY = 100;
	private static final String AGENT_NAME = "agent";
	private static final String CHANNEL_NAME = "channel";
	private static final String FAKE_CHANNEL_NAME = "fakechannel";
	
	private static PlatformContainer platformContainer;
	
	private AgentController agent;
	private CommunicationService communicationProvider;

	@Before
	public void setUp() throws ContainerException, StaleProxyException {
		platformContainer = PlatformContainer.create(TEST_PORT);
		agent = platformContainer.createAgent(AGENT_NAME, CommunicationAgent.class, new Object[0]);
		agent.start();
		
		communicationProvider = agent.getO2AInterface(CommunicationService.class);
	}

	@After
	public void tearDown() throws StaleProxyException, InterruptedException, ContainerException {
		agent.kill();
		shortDelay();
		platformContainer.kill();
	}

	@Test
	public void testInvokeHandlerOnSubscribedChannel() throws InterruptedException {
		CallbackCounterHandler<Object> handler = new CallbackCounterHandler<Object>();
		try{
			communicationProvider.openPublicChannel(CHANNEL_NAME,handler);
			shortDelay();
			communicationProvider.sendPublicMessage(CHANNEL_NAME, null);
			shortDelay();
			
			Assert.assertEquals(1, handler.getCallCount());
		}catch(Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testDoNotInvokeHandlerOnOtherChannel() throws InterruptedException {
		CallbackCounterHandler<Object> handler = new CallbackCounterHandler<Object>();
		try{
			communicationProvider.openPublicChannel(CHANNEL_NAME,handler);
			shortDelay();
			communicationProvider.sendPublicMessage(FAKE_CHANNEL_NAME, null);
			shortDelay();
			
			Assert.assertEquals(0, handler.getCallCount());
		}catch(Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void testDoNotInvokeHandlerWhenSubscriptionCancelled() throws InterruptedException {
		CallbackCounterHandler<Object> handler = new CallbackCounterHandler<Object>();
		try{
			CancelAware token = communicationProvider.openPublicChannel(CHANNEL_NAME,handler);
			shortDelay();
			communicationProvider.sendPublicMessage(CHANNEL_NAME, null);
			shortDelay();
			token.cancel();
			shortDelay();
			communicationProvider.sendPublicMessage(CHANNEL_NAME, null);
			shortDelay();
			
			Assert.assertTrue(token.isCancelled());
			Assert.assertEquals(1, handler.getCallCount());
		}catch(Exception ex) {
			ex.printStackTrace();
			Assert.fail();
		}
	}
	
	private static void shortDelay() {
		try {
			Thread.sleep(SHORT_DELAY);
		} catch (InterruptedException ex) {
		}
	}
}
