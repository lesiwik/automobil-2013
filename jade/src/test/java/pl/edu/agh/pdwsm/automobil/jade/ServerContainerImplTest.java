package pl.edu.agh.pdwsm.automobil.jade;

import junit.framework.Assert;
import org.junit.Test;


public class ServerContainerImplTest {

private static final int TEST_PORT = 9999;
	
	@Test
	public void testServerContainerIsNotReadyAfterCreation() throws ContainerException, InterruptedException {
		@SuppressWarnings("unused")
		PlatformContainer platformContainer = PlatformContainer.create(TEST_PORT);
		ServiceContainer serverContainer = PlatformContainer.createServiceContainer(TEST_PORT);
		
		Assert.assertFalse(serverContainer.isReady());
	}
	
	@Test
	public void testServerContainerIsKilledAfterKill() throws ContainerException, InterruptedException {
		@SuppressWarnings("unused")
		PlatformContainer platformContainer = PlatformContainer.create(TEST_PORT);
		ServiceContainer serverContainer = PlatformContainer.createServiceContainer(TEST_PORT);
		
		serverContainer.kill();
		
		Assert.assertTrue(serverContainer.isKilled());
	}
}
