package pl.edu.agh.pdwsm.automobil.jade;

import junit.framework.Assert;
import org.junit.Test;


public class PlatformContainerTest {

	private static final int TEST_PORT = 9999;
	
	@Test
	public void testPlatformContainerIsReadyAfterCreation() throws ContainerException, InterruptedException {
		PlatformContainer platformContainer = PlatformContainer.create(null, TEST_PORT, false);
		
		Assert.assertTrue(platformContainer.isReady());
	}
	
	@Test
	public void testPlatformContainerIsKilledAfterKill() throws ContainerException, InterruptedException {
		PlatformContainer platformContainer = PlatformContainer.create(TEST_PORT);
		
		platformContainer.kill();
		
		Assert.assertTrue(platformContainer.isKilled());
	}
	
	@Test
	public void testPlatformContainerCreatesServerContainer() throws ContainerException, InterruptedException {
		ServiceContainer serverContainer = PlatformContainer.createServiceContainer(TEST_PORT);
		
		Assert.assertNotNull(serverContainer);
	}
}
