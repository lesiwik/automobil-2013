package pl.edu.agh.pdwsm.automobil.jade;

public interface CancelAware {
	boolean isCancelled();
	void cancel();
}
