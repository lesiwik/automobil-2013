package pl.edu.agh.pdwsm.automobil.jade;

import java.io.Serializable;

public interface RequestHandler {
	Serializable handle(Serializable request);
}
