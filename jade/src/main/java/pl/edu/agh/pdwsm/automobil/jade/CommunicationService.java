package pl.edu.agh.pdwsm.automobil.jade;

import java.io.Serializable;
import java.util.Map;

public interface CommunicationService {
    CancelAware startQueryService(String protocol, RequestHandler handler);

    void query(AgentId queryService, String protocol, Long timeout, Serializable queryBody, ResponseHandler handler) throws CommunicationException;

    CancelAware startServiceDiscovery(String serviceName, String serviceType, Map<String, Serializable> properties, NotificationHandler<AgentId[]> handler);

    void searchService(String serviceName, String serviceType, Map<String, Serializable> properties, NotificationHandler<AgentId[]> handler);

    CancelAware registerService(String serviceName, String serviceType, Map<String, Serializable> properties);

    CancelAware openPublicChannel(String channelName, NotificationHandler<Object> handler);

    void sendPublicMessage(String channelName, Serializable payload);

    CancelAware openPrivateChannel(String channelName, NotificationHandler<Object> handler);

    void sendPrivateMessage(AgentId receiver, String channelName, Serializable payload);
}
