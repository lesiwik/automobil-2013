package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.core.ServiceException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.messaging.TopicManagementHelper;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.util.Logger;
import pl.edu.agh.pdwsm.automobil.jade.NotificationHandler;

import java.util.logging.Level;

public class PublicChannelListenerBehaviour extends CyclicBehaviour {

    private static final Logger log = Logger.getJADELogger(PublicChannelListenerBehaviour.class.getName());

    private static final long serialVersionUID = 1L;

    private AID topic;
    private final String channelName;
    private final NotificationHandler<Object> handler;

    public PublicChannelListenerBehaviour(Agent agent, String channelName, NotificationHandler<Object> handler) {
        super(agent);

        this.channelName = channelName;
        this.handler = handler;
    }

    @Override
    public void onStart() {
        try {
            TopicManagementHelper topicHelper = (TopicManagementHelper) myAgent
                    .getHelper(TopicManagementHelper.SERVICE_NAME);
            topic = topicHelper.createTopic(channelName);
            topicHelper.register(topic);
        } catch (ServiceException ex) {
            String errorMessage = String.format("Unable to register topic %s", channelName);
            log.log(Level.SEVERE, errorMessage, ex);
        }
    }

    @Override
    public int onEnd() {
        if (topic == null) {
            return 0;
        }

        try {
            TopicManagementHelper topicHelper = (TopicManagementHelper) myAgent
                    .getHelper(TopicManagementHelper.SERVICE_NAME);
            topicHelper.deregister(topic);
        } catch (ServiceException ex) {
            String errorMessage = String.format("Unable to deregister topic %s", topic);
            log.log(Level.WARNING, errorMessage, ex);
            return 1;
        } finally {
            topic = null;
        }

        return 0;
    }

    @Override
    public void action() {
        ACLMessage msg = myAgent.receive(MessageTemplate.MatchTopic(topic));
        if (msg != null) {
            try {
                handler.OnMessage(msg.getContentObject());
            } catch (UnreadableException ex) {
                String errorMessage = String.format("Message from %s cannot be decoded", msg.getSender());
                log.log(Level.WARNING, errorMessage, ex);
            }
        } else {
            block();
        }
    }
}
