package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;

public final class SendPrivateNotificationBehaviour extends OneShotBehaviour {
	private static final Logger log = Logger.getJADELogger(SendPrivateNotificationBehaviour.class.getName());
	
	private static final long serialVersionUID = 1L;

	private final AID receiver;
	private final String channelName;
	private final Serializable payload;
	
	public SendPrivateNotificationBehaviour(Agent agent, AID receiver, String channelName, Serializable payload) {
		super(agent);
		
		this.channelName = channelName;
		this.receiver = receiver;
		this.payload = payload;
	}

	@Override
	public void action() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setLanguage(channelName);
		try {
			msg.setContentObject(payload);
		} catch (IOException ex) {
			String errorMessage = String.format("Unable to send message to %s. Error during decoding the payload", receiver);
			log.log(Level.SEVERE, errorMessage, ex);
		}
		msg.addReceiver(receiver);
		
		myAgent.send(msg);
	} 
}
