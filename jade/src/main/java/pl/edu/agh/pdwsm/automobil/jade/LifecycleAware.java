package pl.edu.agh.pdwsm.automobil.jade;

public interface LifecycleAware {
	boolean isReady();
	boolean isKilled();
	void kill() throws ContainerException;
}
