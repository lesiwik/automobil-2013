package pl.edu.agh.pdwsm.automobil.jade;

public class CommunicationException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public CommunicationException(String message, Exception innerException) {
		super(message,innerException);
	}
}
