package pl.edu.agh.pdwsm.automobil.jade;

import java.io.Serializable;

public interface ResponseHandler {
	void handleSuccess(Serializable response);
	void handleFailure();
	void handleRefuse();
}
