package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.util.Logger;

import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

public class RegisterServiceBehaviour extends OneShotBehaviour {
	private static final Logger log = Logger.getJADELogger(RegisterServiceBehaviour.class.getName());

	private static final long serialVersionUID = 1L;

	private final String serviceName;
	private final String serviceType;
	private final Map<String, Serializable> properties;

	public RegisterServiceBehaviour(Agent agent, String serviceName, String serviceType, Map<String, Serializable> properties) {
		super(agent);
		
		this.serviceName = serviceName;
		this.serviceType = serviceType;
		this.properties = properties;
	}
	
	@Override
	public void action() {
		DFAgentDescription agentDescription = new DFAgentDescription();
		agentDescription.setName(myAgent.getAID());

		ServiceDescription serviceDescription = new ServiceDescription();
		serviceDescription.setName(serviceName);
		serviceDescription.setType(serviceType);
		serviceDescription.addLanguages(FIPANames.ContentLanguage.FIPA_SL);

		if (properties != null) {
			for (Entry<String, Serializable> entry : properties.entrySet()) {
				serviceDescription.addProperties(new Property(entry.getKey(), entry.getValue()));
			}
		}

		agentDescription.addServices(serviceDescription);

		try {
			DFService.register(myAgent, agentDescription);
		} catch (FIPAException ex) {
			String errorMessage = String.format("Unable to register service %s", serviceName);
			log.log(Level.SEVERE, errorMessage, ex);
		}
	}
}
