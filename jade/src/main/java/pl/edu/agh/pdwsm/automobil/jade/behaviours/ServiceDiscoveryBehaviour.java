package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionInitiator;
import jade.util.Logger;
import pl.edu.agh.pdwsm.automobil.jade.AgentId;
import pl.edu.agh.pdwsm.automobil.jade.NotificationHandler;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

public class ServiceDiscoveryBehaviour extends SubscriptionInitiator {
    private static final long serialVersionUID = 1L;

    private static final Logger log = Logger.getJADELogger(DeregisterServiceBehaviour.class.getName());

    private final String serviceType;
    private final NotificationHandler<AgentId[]> handler;

    public static ServiceDiscoveryBehaviour create(Agent agent, String serviceName, String serviceType,
                                                   Map<String, Serializable> properties, NotificationHandler<AgentId[]> handler) {

        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription templateSd = new ServiceDescription();
        templateSd.setName(serviceName);
        templateSd.setType(serviceType);

        if (properties != null) {
            for (Entry<String, Serializable> entry : properties.entrySet()) {
                templateSd.addProperties(new Property(entry.getKey(), entry.getValue()));
            }
        }

        template.addServices(templateSd);

        SearchConstraints sc = new SearchConstraints();
        sc.setMaxResults(Long.valueOf(-1));

        ACLMessage msg = DFService.createSubscriptionMessage(agent, agent.getDefaultDF(), template, sc);

        return new ServiceDiscoveryBehaviour(agent, msg, serviceType, handler);
    }

    private ServiceDiscoveryBehaviour(Agent a, ACLMessage msg, String serviceType,
                                      NotificationHandler<AgentId[]> handler) {
        super(a, msg);

        this.serviceType = serviceType;
        this.handler = handler;
    }

    protected void handleInform(ACLMessage inform) {
        try {
            DFAgentDescription[] foundAgents = DFService.decodeNotification(inform.getContent());
            List<AgentId> results = new LinkedList<AgentId>();
            for (int agentNumber = 0; agentNumber < foundAgents.length; agentNumber++) {
                AgentId agentId = new AgentId(foundAgents[agentNumber].getName());
                results.add(agentId);
            }
            handler.OnMessage(results.toArray(new AgentId[0]));
        } catch (FIPAException ex) {
            String errorMessage = String.format("Error while searching for agents providing %s service", serviceType);
            log.log(Level.SEVERE, errorMessage, ex);
        }
    }
}
