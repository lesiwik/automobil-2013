package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.core.ServiceException;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.messaging.TopicManagementHelper;
import jade.lang.acl.ACLMessage;
import jade.util.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;

public class SendPublicNotificationBehaviour extends OneShotBehaviour {
	private static final Logger log = Logger.getJADELogger(SendPublicNotificationBehaviour.class.getName());

	private static final long serialVersionUID = 1L;

	private final String channelName;
	private final Serializable payload;

	public SendPublicNotificationBehaviour(Agent agent, String channelName, Serializable payload) {
		super(agent);

		this.channelName = channelName;
		this.payload = payload;
	}

	@Override
	public void action() {
		try {
			TopicManagementHelper topicHelper = (TopicManagementHelper) myAgent
					.getHelper(TopicManagementHelper.SERVICE_NAME);
			AID topic = topicHelper.createTopic(channelName);

			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			try {
				msg.setContentObject(payload);
			} catch (IOException ex) {
				String errorMessage = String.format("Unable to send message to %s. Error during decoding the payload",
                        topic);
				log.log(Level.SEVERE, errorMessage, ex);
			}
			msg.addReceiver(topic);

			myAgent.send(msg);
		} catch (ServiceException ex) {
			String errorMessage = String.format("Unable to create topic to %s.", channelName);
			log.log(Level.SEVERE, errorMessage, ex);
		}
	}
}
