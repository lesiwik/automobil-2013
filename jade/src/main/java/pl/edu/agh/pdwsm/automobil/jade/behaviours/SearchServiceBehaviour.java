package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.util.Logger;
import pl.edu.agh.pdwsm.automobil.jade.AgentId;
import pl.edu.agh.pdwsm.automobil.jade.NotificationHandler;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

public class SearchServiceBehaviour extends OneShotBehaviour {
    private static final Logger log = Logger.getJADELogger(DeregisterServiceBehaviour.class.getName());

    private static final long serialVersionUID = 1L;

    private final String serviceName;
    private final String serviceType;
    private final Map<String, Serializable> properties;
    private final NotificationHandler<AgentId[]> handler;

    public SearchServiceBehaviour(Agent agent, String serviceName, String serviceType,
                                  Map<String, Serializable> properties, NotificationHandler<AgentId[]> handler) {
        super(agent);

        this.serviceName = serviceName;
        this.serviceType = serviceType;
        this.properties = properties;

        this.handler = handler;
    }

    @Override
    public void action() {
        DFAgentDescription agentDescription = new DFAgentDescription();
        ServiceDescription serviceDescription = new ServiceDescription();
        serviceDescription.setName(serviceName);
        serviceDescription.setType(serviceType);

        if (properties != null) {
            for (Entry<String, Serializable> entry : properties.entrySet()) {
                serviceDescription.addProperties(new Property(entry.getKey(), entry.getValue()));
            }
        }

        agentDescription.addServices(serviceDescription);

        try {
            List<AgentId> results = new LinkedList<AgentId>();
            DFAgentDescription[] foundAgents = DFService.search(myAgent, agentDescription);
            for (int agentNumber = 0; agentNumber < foundAgents.length; agentNumber++) {
                AgentId agentId = new AgentId(foundAgents[agentNumber].getName());
                results.add(agentId);
            }
            handler.OnMessage(results.toArray(new AgentId[0]));
        } catch (FIPAException ex) {
            String errorMessage = String.format("Error while searching for agents providing %s service", serviceType);
            log.log(Level.SEVERE, errorMessage, ex);
            handler.OnMessage(new AgentId[0]);
        }
    }
}
