package pl.edu.agh.pdwsm.automobil.jade;

import jade.core.AID;
import jade.core.Agent;

public final class AgentId {
	final AID id;
	
	public static AgentId createFrom(Agent agent) {
		return new AgentId(agent.getAID());
	}
	
	public AgentId(AID id) {
		this.id = id;
	}
	
	
	public AID getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return id.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof AgentId)) {
			return false;
		}
		
		AgentId other = (AgentId) obj;
		if(id == null && other.id == null) {
			return true;
		}
		
		return id.equals(other.getId());
	}

	@Override
	public int hashCode() {
		if(id == null) {
			return 0;
		}
		
		return id.hashCode();
	}
}
