package pl.edu.agh.pdwsm.automobil.jade;

public interface NotificationHandler<T> {
	void OnMessage(T body);
}
