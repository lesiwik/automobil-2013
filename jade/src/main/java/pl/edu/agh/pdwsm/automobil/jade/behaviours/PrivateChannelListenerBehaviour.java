package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.util.Logger;
import pl.edu.agh.pdwsm.automobil.jade.NotificationHandler;

import java.util.logging.Level;

public final class PrivateChannelListenerBehaviour extends CyclicBehaviour {
    private static final Logger log = Logger.getJADELogger(PrivateChannelListenerBehaviour.class.getName());

    private static final long serialVersionUID = 1L;

    private final MessageTemplate template;
    private final NotificationHandler<Object> handler;

    public PrivateChannelListenerBehaviour(Agent agent, String channelName, NotificationHandler<Object> handler) {
        super(agent);

        this.template = MessageTemplate.and(
                MessageTemplate.MatchPerformative(ACLMessage.INFORM),
                MessageTemplate.MatchLanguage(channelName));
        this.handler = handler;
    }

    @Override
    public void action() {
        ACLMessage msg = myAgent.receive(template);
        if (msg != null) {
            try {
                handler.OnMessage(msg.getContentObject());
            } catch (UnreadableException ex) {
                String errorMessage = String.format("Message from %s cannot be decoded", msg.getSender());
                log.log(Level.WARNING, errorMessage, ex);
            }
        } else {
            block();
        }
    }
}
