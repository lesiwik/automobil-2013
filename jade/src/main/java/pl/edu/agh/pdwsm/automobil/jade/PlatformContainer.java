package pl.edu.agh.pdwsm.automobil.jade;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.core.Runtime;

public final class PlatformContainer extends BaseContainer implements LifecycleAware {

	private PlatformContainer(AgentContainer mainContainer) {
		super(mainContainer);
	}

	public static ServiceContainer createServiceContainer(int port) {
		ProfileImpl containerProfile = new ProfileImpl(null, port, null);
		AgentContainer container = getRuntime().createAgentContainer(containerProfile);

		return new BaseContainer(container);
	}

	private static Runtime getRuntime() {
		Runtime runtime = Runtime.instance();
		runtime.setCloseVM(false);
		return runtime;
	}

	public static PlatformContainer create(int port) throws ContainerException {
		return create(null, port, false);
	}

	public static PlatformContainer create(String host, int port) throws ContainerException {
		return create(host, port, false);
	}

	public static PlatformContainer create(String host, int port, boolean withGui) throws ContainerException {
		try {
			Profile mainProfile = new ProfileImpl(null, port, null);
			mainProfile.setParameter(Profile.SERVICES, Profile.DEFAULT_SERVICES_NOMOBILITY + ";"
					+ jade.core.messaging.TopicManagementService.class.getName());
			AgentContainer mainContainer = getRuntime().createMainContainer(mainProfile);

			if (withGui) {
				AgentController rma = mainContainer.createNewAgent("rma", "jade.tools.rma.rma", new Object[0]);
				rma.start();
			}
			return new PlatformContainer(mainContainer);
		} catch (Exception ex) {
			throw new ContainerException("Unable to create platform container on specified port", ex);
		}
	}
}
