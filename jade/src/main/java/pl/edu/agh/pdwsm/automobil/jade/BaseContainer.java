package pl.edu.agh.pdwsm.automobil.jade;

import jade.core.Agent;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;

class BaseContainer implements ServiceContainer {
	private static final String COMMUNICATION_SERVICE = "CommunicationService";

	protected final AgentContainer container;

	private AgentController communicationAgent;
	private CommunicationService communicationService;

	public BaseContainer(AgentContainer mainContainer) {
		this.container = mainContainer;
	}

	@Override
	public void kill() throws ContainerException {
		try {
			container.kill();
		} catch (Exception ex) {
			throw new ContainerException("Error during tansition to killed state", ex);
		}
	}

	@Override
	public boolean isReady() {
		return container.getState().getCode() == 4;
	}

	@Override
	public boolean isKilled() {
		return container.getState().getCode() == 8;
	}

	public AgentController createAgent(String name, Class<?> agentClass, Object[] args) throws ContainerException {
		try {
			return this.container.createNewAgent(name, agentClass.getName(), args);
		} catch (Exception ex) {
			String errorMessage = String.format("Unable to create agent %s of type %s", name, Agent.class.getName());
			throw new ContainerException(errorMessage, ex);
		}
	}

	public synchronized CommunicationService getCommunicationService() throws ContainerException {
		if (communicationAgent == null) {
			communicationAgent = createAgent(COMMUNICATION_SERVICE, CommunicationAgent.class, new Object[0]);
			try {
				communicationService = communicationAgent.getO2AInterface(CommunicationService.class);
			} catch (Exception ex) {
				throw new ContainerException("Unable to instantiate communication service", ex);
			}
		}
		
		return communicationService;
	}
}
