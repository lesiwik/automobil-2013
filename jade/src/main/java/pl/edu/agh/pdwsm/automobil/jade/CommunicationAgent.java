package pl.edu.agh.pdwsm.automobil.jade;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import pl.edu.agh.pdwsm.automobil.jade.behaviours.*;

import java.io.Serializable;
import java.util.Map;

public class CommunicationAgent extends Agent implements CommunicationService {

	private static final long serialVersionUID = 1L;

	public CommunicationAgent() {
		registerO2AInterface(CommunicationService.class, this);
	}

	@Override
	public void sendPrivateMessage(AgentId receiver, String channelName, Serializable payload) {
		Behaviour behavior = new SendPrivateNotificationBehaviour(this, receiver.getId(), channelName, payload);
		addBehaviour(behavior);
	}

	@Override
	public CancelAware openPrivateChannel(String channelName, NotificationHandler<Object> handler) {
		Behaviour behaviour = new PrivateChannelListenerBehaviour(this, channelName, handler);
		addBehaviour(behaviour);

		return cancelByRemove(behaviour);
	}

	@Override
	public CancelAware openPublicChannel(String channelName, NotificationHandler<Object> handler) {
		Behaviour behaviour = new PublicChannelListenerBehaviour(this, channelName, handler);
		addBehaviour(behaviour);

		return cancelByRemove(behaviour);
	}

	@Override
	public void sendPublicMessage(String channelName, Serializable payload) {
		Behaviour behavior = new SendPublicNotificationBehaviour(this, channelName, payload);
		addBehaviour(behavior);
	}

	@Override
	public CancelAware registerService(String serviceName, String serviceType, Map<String, Serializable> properties) {
		Behaviour behavior = new RegisterServiceBehaviour(this, serviceName, serviceType, properties);
		addBehaviour(behavior);

		Behaviour cancelBehaviour = new DeregisterServiceBehaviour(this, serviceName, serviceType, properties);
		return cancelByExecute(cancelBehaviour);
	}

	@Override
	public void searchService(String serviceName, String serviceType, Map<String, Serializable> properties,
			NotificationHandler<AgentId[]> handler) {
		Behaviour behavior = new SearchServiceBehaviour(this, serviceName, serviceType, properties, handler);
		addBehaviour(behavior);
	}

	@Override
	public CancelAware startServiceDiscovery(String serviceName, String serviceType,
			Map<String, Serializable> properties, NotificationHandler<AgentId[]> handler) {
		Behaviour behavior = ServiceDiscoveryBehaviour.create(this, serviceName, serviceType, properties, handler);
		addBehaviour(behavior);

		return cancelByRemove(behavior);
	}

	@Override
	public void query(AgentId queryService, String protocol, Long timeout, Serializable queryBody,
			ResponseHandler handler) throws CommunicationException {
		Behaviour behavior = RequestInitiatorBehaviour
				.create(this, queryService, protocol, timeout, queryBody, handler);
		addBehaviour(behavior);
	}

	@Override
	public CancelAware startQueryService(String protocol, RequestHandler handler) {
		Behaviour behavior = RequestResponderBehaviour.create(this, protocol, handler);
		addBehaviour(behavior);

		return cancelByRemove(behavior);
	}

	private CancelAware cancelByRemove(Behaviour behavior) {
		return new CancelToken(behavior, true);
	}

	private CancelAware cancelByExecute(Behaviour behavior) {
		return new CancelToken(behavior, false);
	}

	private class CancelToken implements CancelAware {
		private final Behaviour behaviour;
		private final boolean cancelByRemove;
		private boolean isCancelled;

		public CancelToken(Behaviour behaviour, boolean cancelByRemove) {
			this.behaviour = behaviour;
			this.cancelByRemove = cancelByRemove;
			this.isCancelled = false;
		}

		@Override
		public synchronized boolean isCancelled() {
			return isCancelled;
		}

		@Override
		public synchronized void cancel() {
			if (isCancelled) {
				return;
			}

			try {
				if (cancelByRemove) {
					removeBehaviour(behaviour);
				} else {
					addBehaviour(behaviour);
				}
			} finally {
				isCancelled = true;
			}
		}
	}
}
