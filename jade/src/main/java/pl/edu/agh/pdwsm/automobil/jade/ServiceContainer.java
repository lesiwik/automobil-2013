package pl.edu.agh.pdwsm.automobil.jade;

public interface ServiceContainer extends LifecycleAware {
	CommunicationService getCommunicationService() throws ContainerException;
}
