package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.Agent;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.proto.AchieveREResponder;
import jade.util.Logger;
import pl.edu.agh.pdwsm.automobil.jade.RequestHandler;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;

public class RequestResponderBehaviour extends AchieveREResponder {
	private static final Logger log = Logger.getJADELogger(RequestResponderBehaviour.class.getName());
	
	private static final long serialVersionUID = 1L;

	private final RequestHandler requestHandler;

	public static RequestResponderBehaviour create(Agent agent, String protocol, RequestHandler requestHandler) {
		MessageTemplate template = MessageTemplate.and(MessageTemplate.MatchProtocol(protocol),
				MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

		return new RequestResponderBehaviour(agent, template, requestHandler);
	}

	private RequestResponderBehaviour(Agent agent, MessageTemplate template, RequestHandler requestHandler) {
		super(agent, template);

		this.requestHandler = requestHandler;
	}

	@Override
	protected ACLMessage prepareResponse(ACLMessage request) throws NotUnderstoodException, RefuseException {
		Serializable requestBody;
		try {
			requestBody = request.getContentObject();
		} catch (UnreadableException ex) {
			String errorMessage = String.format("Could not decode the body of the request from %s", request.getSender());
			log.log(Level.WARNING, errorMessage, ex);
			throw new NotUnderstoodException("Could not decode the body of request");
		}

		Serializable response = requestHandler.handle(requestBody);
		if (response != null) {
			ACLMessage responseMessage = request.createReply();
			try {
				responseMessage.setContentObject(response);
			} catch (IOException ex) {
				String errorMessage = String.format("Could not decode the body of the response to %s", request.getSender());
				log.log(Level.SEVERE, errorMessage, ex);
				responseMessage.setPerformative(ACLMessage.FAILURE);
			}
			responseMessage.setPerformative(ACLMessage.INFORM);
			return responseMessage;
		}

		String errorMessage = String.format("Could not process the body of the request from %s", request.getSender());
		log.log(Level.WARNING, errorMessage);
		throw new RefuseException("Could not process the body of the request");
	}
}
