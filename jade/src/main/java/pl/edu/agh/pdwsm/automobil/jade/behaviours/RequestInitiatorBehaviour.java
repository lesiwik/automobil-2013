package pl.edu.agh.pdwsm.automobil.jade.behaviours;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.proto.AchieveREInitiator;
import jade.util.Logger;
import pl.edu.agh.pdwsm.automobil.jade.AgentId;
import pl.edu.agh.pdwsm.automobil.jade.CommunicationException;
import pl.edu.agh.pdwsm.automobil.jade.ResponseHandler;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;

public class RequestInitiatorBehaviour extends AchieveREInitiator {
    private static final Logger log = Logger.getJADELogger(RequestInitiatorBehaviour.class.getName());

    private static final long serialVersionUID = 1L;

    private final ResponseHandler handler;

    public static RequestInitiatorBehaviour create(Agent agent, AgentId receiver, String protocol, Long timeout,
                                                   Serializable requestBody, ResponseHandler hanlder) throws CommunicationException {

        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(receiver.getId());
        msg.setProtocol(protocol);

        if (timeout != null) {
            msg.setReplyByDate(new Date(System.currentTimeMillis() + timeout));
        }

        try {
            msg.setContentObject(requestBody);
        } catch (IOException ex) {
            String errorMessage = "Unable to encode the body of the request message";
            throw new CommunicationException(errorMessage, ex);
        }

        return new RequestInitiatorBehaviour(agent, msg, hanlder);
    }

    private RequestInitiatorBehaviour(Agent agent, ACLMessage msg, ResponseHandler handler) {
        super(agent, msg);

        this.handler = handler;
    }

    protected long handleTimeout() {
        handler.handleFailure();
        return 0;
    }

    protected void handleAllResponses(@SuppressWarnings("rawtypes") java.util.Vector responses) {
        if (responses.size() == 0) { //timeout
            handler.handleRefuse();
        } else {
            for (Object msg : responses) {
                if (msg instanceof ACLMessage) {
                    switch (((ACLMessage) msg).getPerformative()) {
                        case ACLMessage.INFORM:
                            handleInform((ACLMessage) msg);
                        case ACLMessage.REFUSE:
                            handleRefuse((ACLMessage) msg);
                        case ACLMessage.FAILURE:
                        default:
                            handleFailure((ACLMessage) msg);
                    }
                }
            }
        }
    }

    protected void handleInform(ACLMessage inform) {
        try {
            Serializable responseBody = inform.getContentObject();

            String logMessage = String.format("Agent %s successfully performed the request", inform.getSender()
                    .getName());
            log.log(Level.FINE, logMessage);

            handler.handleSuccess(responseBody);
        } catch (UnreadableException ex) {
            String errorMessage = String.format("Unable to decode the response body from %s", inform.getSender()
                    .getName());
            log.log(Level.SEVERE, errorMessage);

            handler.handleFailure();
        }
    }

    protected void handleRefuse(ACLMessage refuse) {
        String errorMessage = String.format("Agent %s refused to perform the action", refuse.getSender().getName());
        log.log(Level.SEVERE, errorMessage);

        handler.handleRefuse();
    }

    protected void handleFailure(ACLMessage failure) {
        if (failure.getSender().equals(myAgent.getAMS())) {
            log.log(Level.SEVERE, "Responder does not exist");
        } else {
            String errorMessage = String.format("Agent %s failed to perform the action", failure.getSender().getName());
            log.log(Level.SEVERE, errorMessage);
        }

        handler.handleFailure();
    }
}
