package pl.edu.agh.pdwsm.automobil.jade;

public class ContainerException extends Exception { // extends CommunicationException {
	private static final long serialVersionUID = 1L;
	
	public ContainerException(String message, Exception innerException) {
		super(message, innerException);
	}
}
