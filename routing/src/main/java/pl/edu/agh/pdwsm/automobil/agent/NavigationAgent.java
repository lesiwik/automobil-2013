package pl.edu.agh.pdwsm.automobil.agent;

import edu.uci.ics.jung.graph.Graph;
import jade.content.ContentManager;
import jade.content.lang.sl.SLCodec;
import jade.core.Agent;
import org.joda.time.DateTime;
import pl.edu.agh.pdwsm.automobil.PrivilegedRoutingService;
import pl.edu.agh.pdwsm.automobil.RoutingService;
import pl.edu.agh.pdwsm.automobil.agent.behaviours.*;
import pl.edu.agh.pdwsm.automobil.model.RoutingTable;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertices;
import pl.edu.agh.pdwsm.automobil.ontology.AutomobilOntology;
import pl.edu.agh.pdwsm.automobil.ontology.Position;

import java.util.List;

public class NavigationAgent extends Agent implements MobileAgent {

    protected RoutingService routingService;

    private final PositionNotification positionNotification;

    private MapInquiry mapInquiry;

    private RoadTrafficInquirer roadTrafficInquirer;
    private RoadTrafficListener roadTrafficListener;
    private RoadTrafficResponder roadTrafficResponder;

    public NavigationAgent() {
        registerO2AInterface(MobileAgent.class, this);
        positionNotification = new PositionNotification(this);
        mapInquiry = new MapInquiry(this);
    }

    @Override
    protected void setup() {
        ContentManager cm = getContentManager();
        cm.registerLanguage(new SLCodec());
        cm.registerOntology(AutomobilOntology.getInstance());
        cm.setValidationMode(false);
        addBehaviour(positionNotification);
        roadTrafficInquirer = new RoadTrafficInquirer(this, routingService);
        addBehaviour(roadTrafficInquirer);
        roadTrafficListener = new RoadTrafficListener(this, routingService);
        addBehaviour(roadTrafficListener);
        roadTrafficResponder = new RoadTrafficResponder(this, routingService);
        addBehaviour(roadTrafficResponder);
        addBehaviour(mapInquiry);
    }

    @Override
    public void startRouting(Position startPoint, Position endPoint, boolean privileged) {
        Vertex vertexStart = Vertices.newVertex(startPoint);
        Vertex vertexEnd = Vertices.newVertex(endPoint);
        setupRoutingService(vertexStart, vertexEnd, privileged);
        routingService.startRouting(vertexStart, vertexEnd);
    }

    private void setupRoutingService(Vertex vertexStart, Vertex vertexEnd, boolean privileged) {
        Graph<Vertex, Edge> map = mapInquiry.getMap(vertexStart, vertexEnd);
        if (privileged) {
            routingService = new PrivilegedRoutingService(new RoutingTable(map), 5000, mapInquiry);
        } else {
            routingService = new RoutingService(new RoutingTable(map), 5000, mapInquiry);
        }
        roadTrafficListener.setRoutingService(routingService);
        roadTrafficInquirer.setRoutingService(routingService);
        roadTrafficResponder.setRoutingService(routingService);
    }

    @Override
    public List<Edge> currentPath() {
        return routingService.getCurrentPath();
    }

    @Override
    public void updatePosition(Position position) {
        if (routingService != null) {
            routingService.updatePosition(position);
        }
        positionNotification.updateLastKnownPosition(position);
    }

    @Override
    public Position currentPosition() {
        if (routingService != null) {
            return routingService.currentPosition();
        }
        return new Position(0, 0, DateTime.now());
    }

    @Override
    public boolean isNavigating() {
        return routingService != null && (!routingService.getCurrentPath().isEmpty());
    }
}
