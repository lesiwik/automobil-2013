package pl.edu.agh.pdwsm.automobil.agent.behaviours;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import pl.edu.agh.pdwsm.automobil.RoutingService;
import pl.edu.agh.pdwsm.automobil.agent.behaviours.AbstractJadeBehaviour;
import pl.edu.agh.pdwsm.automobil.traffic.TrafficInfo;

import java.util.logging.Level;

public class RoadTrafficListener extends AbstractJadeBehaviour {
    MessageTemplate positionTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchConversationId(AbstractJadeBehaviour.TRAFFIC_INFO_RESPONSE));

    private RoutingService routingService;

    public RoadTrafficListener(Agent a, RoutingService routingService) {
        super(a);
        this.setRoutingService(routingService);
    }

    @Override
    public void action() {
        try {
            ACLMessage msg = myAgent.receive(positionTemplate);
            if (msg != null) {
                TrafficInfo trafficInfo = (TrafficInfo) msg.getContentObject();
                getRoutingService().updateRoutingTable(trafficInfo.getRoutingTableUpdate());
                logger.log(Level.INFO, "Traffic info update " + trafficInfo);
            } else {
                block();
            }
        } catch (UnreadableException e) {
            e.printStackTrace();
        }
    }

    public RoutingService getRoutingService() {
        return routingService;
    }

    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }
}