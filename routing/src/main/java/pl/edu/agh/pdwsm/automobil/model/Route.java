package pl.edu.agh.pdwsm.automobil.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;

public class Route implements Serializable {
    private final List<Vertex> route = new ArrayList<Vertex>();

    public Route(List<Edge> edges) {
        for (Edge edge : edges) {
            route.add(edge.getStartPoint());
        }
    }
}
