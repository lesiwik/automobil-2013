package pl.edu.agh.pdwsm.automobil;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getLast;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import pl.edu.agh.pdwsm.automobil.agent.behaviours.MapInquiry;
import pl.edu.agh.pdwsm.automobil.model.DistancesAndTimesStatistics;
import pl.edu.agh.pdwsm.automobil.model.RoutingTable;
import pl.edu.agh.pdwsm.automobil.model.RoutingTableUpdate;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertices;
import pl.edu.agh.pdwsm.automobil.ontology.DistanceCalculator;
import pl.edu.agh.pdwsm.automobil.ontology.Position;

public class RoutingService {
    private RoutingTable routingTable;
    private List<Edge> currentPath = new ArrayList<Edge>();
    private Position lastPosition = new Position();
    private final double queryDistance;
    protected final MapInquiry mapInquiry;
    private final DistancesAndTimesStatistics statistics = new DistancesAndTimesStatistics();

    public RoutingService(RoutingTable routingTable, double queryDistance, MapInquiry mapInquiry) {
        this.routingTable = routingTable;
        this.queryDistance = queryDistance;
        this.mapInquiry = mapInquiry;
    }

    public void startRouting(Vertex startPoint, Vertex endPoint) {
        currentPath = routingTable.calculateRoute(startPoint, endPoint);
        lastPosition = startPoint.toPosition();
    }

    public synchronized void updatePosition(Position currentPosition) {
        int stepDurationInSeconds = getStepDuration(currentPosition);
        double distance = currentPosition.getSpeed() * stepDurationInSeconds;
        statistics.updateWith(distance, stepDurationInSeconds);
        Position predictedPosition = calculateNewPosition(distance, currentPosition.getTimestamp());
        if (!onRoute(currentPosition, predictedPosition)) {
            startNewRoutingFromPosition(currentPosition);
        }
        lastPosition = currentPosition;
    }

    private void startNewRoutingFromPosition(Position currentPosition) {
        Vertex start = Vertices.newVertex(currentPosition);
        Vertex end = getLast(currentPath).getEndPoint();
        routingTable = new RoutingTable(mapInquiry.getMap(start, end));
        startRouting(start, end);
    }

    private int getStepDuration(Position currentPosition) {
        Duration d = new Duration(lastPosition.getTimestamp(), currentPosition.getTimestamp());
        int stepDurationInSeconds = d.toStandardSeconds().getSeconds();
        return stepDurationInSeconds;
    }

    private boolean onRoute(Position currentPosition, Position predictedPosition) {
        return DistanceCalculator.calculateDistance(currentPosition, predictedPosition) < 10;
    }

    private Position calculateNewPosition(double currentlyPassedDistance, DateTime newTime) {
        Edge currentSegment = currentPath.remove(0);
        Position position = lastPosition.shift(0.0, 0.0, lastPosition.getTimestamp());
        boolean passedSegmentBoundaries = false;
        double remainingDistance;
        while (currentlyPassedDistance > (remainingDistance = DistanceCalculator.calculateDistance(position, currentSegment.getEndPoint()
                .toPosition()))) {
            currentlyPassedDistance -= remainingDistance;
            position = currentSegment.getEndPoint().toPosition();
            passedSegmentBoundaries = true;
            currentSegment.setWeight(currentSegment.getLength() / statistics.getAverageSpeed());
            if (!currentPath.isEmpty()) {
                currentSegment = currentPath.remove(0);
            } else {
                break;
            }
        }
        if (!(currentlyPassedDistance > 0 && currentPath.size() == 0)) {
            currentPath.add(0, currentSegment);
        }
        if (passedSegmentBoundaries) {
            statistics.reset();
        }
        lastPosition = calculateNewPositionOnSegment(position, currentlyPassedDistance, currentSegment, newTime);
        return lastPosition;
    }

    private Position calculateNewPositionOnSegment(Position position, double currentlyPassedDistance, Edge currentSegment, DateTime newTime) {
        double ratio = currentlyPassedDistance / currentSegment.getLength();
        double latitude = currentSegment.getEndPoint().getLatitude() - currentSegment.getStartPoint().getLatitude();
        latitude *= ratio;
        double longitude = currentSegment.getEndPoint().getLongitude() - currentSegment.getStartPoint().getLongitude();
        longitude *= ratio;
        position = position.shift(latitude, longitude, newTime);
        return position;
    }

    public synchronized void updateRoutingTable(RoutingTableUpdate routingUpdate) {
        routingTable.update(routingUpdate);
        List<Edge> possiblyUpdatedPart = getQueryPath();
        if (isNotEmpty(possiblyUpdatedPart)) {
            Edge currentSegment = getFirst(possiblyUpdatedPart, null);
            Vertex currentFirst = currentSegment.getStartPoint();
            List<Edge> newRoute = routingTable.calculateRoute(currentFirst, getLast(possiblyUpdatedPart).getEndPoint());
            if (newRoute.contains(currentSegment)) { // only if not reverse
                newRoute.addAll(currentPath.subList(findEdge(getLast(newRoute).getEndPoint()) + 1, currentPath.size()));
                currentPath = newRoute;
            }
        }
    }

    public synchronized List<Edge> getQueryPath() {
        double distance = 0;
        List<Edge> path = new ArrayList<Edge>();
        for (Edge edge : currentPath) {
            path.add(edge);
            distance += edge.getLength();
            if (distance > queryDistance) {
                return path;
            }
        }
        return path;
    }

    private int findEdge(Vertex point) {
        for (int i = 0; i < currentPath.size(); i++) {
            if (currentPath.get(i).getEndPoint().equals(point))
                return i;
        }
        return -1;
    }

    public synchronized RoutingTableUpdate calculateQueryResponse(List<Edge> queryPath) {
        return new RoutingTableUpdate(routingTable.findQueryPath(queryPath), routingTable.calculateRoute(queryPath.get(0).getStartPoint(),
                getLast(queryPath).getEndPoint()));
    }

    /**
     * Necessary for simulation
     */
    public synchronized List<Edge> getCurrentPath() {
        return currentPath;
    }

    public synchronized Position currentPosition() {
        return lastPosition;
    }

}
