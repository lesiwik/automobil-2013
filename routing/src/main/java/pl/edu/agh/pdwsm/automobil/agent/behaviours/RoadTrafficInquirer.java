package pl.edu.agh.pdwsm.automobil.agent.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.Iterator;
import pl.edu.agh.pdwsm.automobil.RoutingService;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.traffic.TrafficQuery;

import java.util.List;

public class RoadTrafficInquirer extends AbstractJadeBehaviour {
    public static final String CLOSE_AGENTS_CONVERSATION_ID = "close-agents";

    MessageTemplate positionTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchConversationId(CLOSE_AGENTS_CONVERSATION_ID));

    private RoutingService routingService;

    public RoadTrafficInquirer(Agent a, RoutingService routingService) {
        super(a);
        this.setRoutingService(routingService);
    }

    @Override
    public void onStart() {
        ACLMessage subscription = createMessage(ACLMessage.SUBSCRIBE, CLOSE_AGENTS_CONVERSATION_ID);
        subscription.addReceiver(new AID(AbstractJadeBehaviour.AGENT_MANAGER, AID.ISLOCALNAME));
        myAgent.send(subscription);
    }

    @Override
    public void action() {
        ACLMessage msg = myAgent.receive(positionTemplate);
        if (msg != null) {
            try {
                Iterator receivers = msg.getAllReceiver();
                while (receivers.hasNext()) {
                    AID next = (AID) receivers.next();
                    if (!next.equals(myAgent.getAID())) {
                        // logger.log(Level.INFO, "Near agent: " + next);

                        ACLMessage message = createMessage(ACLMessage.INFORM, AbstractJadeBehaviour.TRAFFIC_INFO_QUERY);
                        message.addReceiver(next);
                        if (routingService != null) {
                            List<Edge> queryPath = routingService.getQueryPath();
                            if (!queryPath.isEmpty()) {
                                message.setContentObject(new TrafficQuery(queryPath));
                                myAgent.send(message);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                jade.util.Logger.println(e.toString());
                e.printStackTrace();
            }
        } else {
            block();
        }
    }

    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }
}