package pl.edu.agh.pdwsm.automobil.model;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;

import java.io.Serializable;
import java.util.List;

public class RoutingTableUpdate implements Serializable{
    private final List<Edge> currentPath;
    private final List<Edge> optimalPath;

    public RoutingTableUpdate(List<Edge> currentPath, List<Edge> optimalPath) {
        this.currentPath = currentPath;
        this.optimalPath = optimalPath;
    }

    public List<Edge> getCurrentPath() {
        return currentPath;
    }

    public List<Edge> getOptimalPath() {
        return optimalPath;
    }
}
