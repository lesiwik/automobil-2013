package pl.edu.agh.pdwsm.automobil.agent.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.io.Serializable;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.MapQuery;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;
import edu.uci.ics.jung.graph.Graph;

public class MapInquiry extends AbstractJadeBehaviour {

    MessageTemplate positionTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchConversationId(AbstractJadeBehaviour.MAP_RESPONSE));

    public MapInquiry(Agent a) {
        super(a);
    }

    @Override
    public void action() {
        block();
    }

    public Graph<Vertex, Edge> getMap(Vertex start, Vertex end) {
        sendRequest(start, end);
        return getAnswer();
    }

    private void sendRequest(Vertex start, Vertex end) {
        try {
            ACLMessage mapQuery = createMessage(ACLMessage.INFORM, AbstractJadeBehaviour.MAP_QUERY);
            mapQuery.addReceiver(new AID(AbstractJadeBehaviour.MAP_PROVIDER, AID.ISLOCALNAME));
            mapQuery.setContentObject(new MapQuery(start, end));
            myAgent.send(mapQuery);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Graph<Vertex, Edge> getAnswer() {
        Graph<Vertex, Edge> graph = null;
        while (true) {
            try {
                ACLMessage msg = myAgent.receive(positionTemplate);
                if (msg != null) {
                    graph = (Graph<Vertex, Edge>) msg.getContentObject();
                    break;
                } else {
                    Thread.sleep(1000);
                }
            } catch (UnreadableException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return graph;
    }
}
