package pl.edu.agh.pdwsm.automobil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;

import pl.edu.agh.pdwsm.automobil.agent.behaviours.MapInquiry;
import pl.edu.agh.pdwsm.automobil.model.RoutingTable;
import pl.edu.agh.pdwsm.automobil.model.RoutingTableUpdate;
import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.UpdateSourceType;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

public class PrivilegedRoutingService extends RoutingService {

    private static final int CLEARING_MULTIPLIER = 2;

    public PrivilegedRoutingService(RoutingTable routingTable, double queryDistance, MapInquiry mapInquiry) {
        super(routingTable, queryDistance, mapInquiry);
    }

    @Override
    public synchronized void updateRoutingTable(RoutingTableUpdate routingUpdate) {
        RoutingTableUpdate filteredRoutingUpdate = removePrivilagedUpdates(routingUpdate);
        super.updateRoutingTable(filteredRoutingUpdate);
    }

    private RoutingTableUpdate removePrivilagedUpdates(RoutingTableUpdate routingUpdate) {
        List<Edge> fileredCurrentPath = filterPath(routingUpdate.getCurrentPath());
        List<Edge> filteredOptimalPath = filterPath(routingUpdate.getOptimalPath());
        return new RoutingTableUpdate(fileredCurrentPath, filteredOptimalPath);
    }

    private List<Edge> filterPath(List<Edge> path) {
        return new ArrayList<Edge>(Collections2.filter(path, new Predicate<Edge>() {
            @Override
            public boolean apply(Edge e) {
                return e.getLastUpdateType().equals(UpdateSourceType.NORMAL);
            }
        }));
    }

    @Override
    public synchronized RoutingTableUpdate calculateQueryResponse(List<Edge> queryPath) {
        RoutingTableUpdate result = super.calculateQueryResponse(queryPath);
        List<Edge> clearedCurrentPath = clearPath(result.getCurrentPath());
        List<Edge> clearedOptimalPath = clearPath(result.getOptimalPath());
        return new RoutingTableUpdate(clearedCurrentPath, clearedOptimalPath);
    }

    public synchronized List<Edge> clearPath(List<Edge> queryPath) {
        final Set<Edge> currentPath = new HashSet<Edge>(getCurrentPath());
        Collection<Edge> clearedPath = Collections2.transform(queryPath, new Function<Edge, Edge>() {
            @Override
            public Edge apply(Edge input) {
                Edge newEdge = new Edge(input.getStartPoint(), input.getEndPoint(), input.getLength(), input.getPerfectSpeed());
                newEdge.setTimestamp(new DateTime());
                if (currentPath.contains(input)) {
                    newEdge.setWeight(input.getWeight() * CLEARING_MULTIPLIER);
                    newEdge.setLastUpdateType(UpdateSourceType.PRIVILEGED);
                } else {
                    newEdge.setWeight(input.getWeight());
                }
                return newEdge;
            }
        });
        return new ArrayList<Edge>(clearedPath);
    }
}
