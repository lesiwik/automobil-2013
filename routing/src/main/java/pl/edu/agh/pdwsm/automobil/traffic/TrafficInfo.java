package pl.edu.agh.pdwsm.automobil.traffic;

import java.io.Serializable;

import pl.edu.agh.pdwsm.automobil.model.RoutingTableUpdate;

public class TrafficInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private RoutingTableUpdate routingTableUpdate;

    public TrafficInfo(RoutingTableUpdate routingTableUpdate) {
        this.routingTableUpdate = routingTableUpdate;
    }

    public RoutingTableUpdate getRoutingTableUpdate() {
        return routingTableUpdate;
    }
}
