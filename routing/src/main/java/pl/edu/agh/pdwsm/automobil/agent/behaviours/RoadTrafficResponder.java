package pl.edu.agh.pdwsm.automobil.agent.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.logging.Level;

import pl.edu.agh.pdwsm.automobil.RoutingService;
import pl.edu.agh.pdwsm.automobil.model.RoutingTableUpdate;
import pl.edu.agh.pdwsm.automobil.traffic.TrafficInfo;
import pl.edu.agh.pdwsm.automobil.traffic.TrafficQuery;

public class RoadTrafficResponder extends AbstractJadeBehaviour {
    MessageTemplate trafficQueryTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchConversationId(AbstractJadeBehaviour.TRAFFIC_INFO_QUERY));

    private RoutingService routingService;

    public RoadTrafficResponder(Agent a, RoutingService routingService) {
        super(a);
        this.setRoutingService(routingService);
    }

    @Override
    public void action() {
        try {
            ACLMessage msg = myAgent.receive(trafficQueryTemplate);
            if (msg != null) {
                TrafficQuery trafficQuery = (TrafficQuery) msg.getContentObject();
                RoutingTableUpdate routingTableUpdate = getRoutingService().calculateQueryResponse(trafficQuery.getQueryPath());

                AID sender = msg.getSender();
                logger.log(Level.INFO, "Responding with info to: " + sender + " with: " + routingTableUpdate);
                ACLMessage message = createMessage(ACLMessage.INFORM, AbstractJadeBehaviour.TRAFFIC_INFO_RESPONSE);
                message.addReceiver(sender);
                message.setContentObject(new TrafficInfo(routingTableUpdate));
                myAgent.send(message);
            } else {
                block();
            }
        } catch (UnreadableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public RoutingService getRoutingService() {
        return routingService;
    }

    public void setRoutingService(RoutingService routingService) {
        this.routingService = routingService;
    }
}
