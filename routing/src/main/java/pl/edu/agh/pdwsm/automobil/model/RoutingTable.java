package pl.edu.agh.pdwsm.automobil.model;

import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.transform;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections15.Transformer;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.model.graph.Vertex;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.graph.Graph;

public class RoutingTable {

    private final Graph<Vertex, Edge> area;

    public RoutingTable(Graph<Vertex, Edge> area) {
        this.area = area;
    }

    public void update(RoutingTableUpdate routingUpdate) {
        update(routingUpdate.getCurrentPath());
        update(routingUpdate.getOptimalPath());
    }

    private void update(List<Edge> path) {
        for (Edge edgeUpdate : path) {
            Edge areaEdge = area.findEdge(edgeUpdate.getStartPoint(), edgeUpdate.getEndPoint());
            if (areaEdge != null && areaEdge.getTimestamp().isBefore(edgeUpdate.getTimestamp())) {
                areaEdge.setWeight(edgeUpdate.getWeight());
                areaEdge.setTimestamp(edgeUpdate.getTimestamp());
                areaEdge.setLastUpdateType(edgeUpdate.getLastUpdateType());
            }
        }
    }

    public List<Edge> calculateRoute(Vertex startPoint, Vertex endPoint) {
        DijkstraShortestPath<Vertex, Edge> paths = new DijkstraShortestPath<Vertex, Edge>(area, new Transformer<Edge, Number>() {
            @Override
            public Number transform(Edge edge) {
                return edge.getWeight();
            }
        });
        try {
            return paths.getPath(startPoint, endPoint);
        } catch (IllegalArgumentException ex) {
            return Collections.emptyList();
        }
    }

    public List<Edge> findQueryPath(List<Edge> queryPath) {
        List<Edge> path = transform(queryPath, new Function<Edge, Edge>() {
            @Override
            public Edge apply(Edge input) {
                return area.findEdge(input.getStartPoint(), input.getEndPoint());
            }
        });
        Collection<Edge> filteredPath = filter(path, new Predicate<Edge>() {
            @Override
            public boolean apply(Edge e) {
                return e != null;
            }
        });
        return newArrayList(filteredPath);
    }
}
