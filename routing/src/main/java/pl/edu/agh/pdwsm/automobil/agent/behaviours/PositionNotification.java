package pl.edu.agh.pdwsm.automobil.agent.behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

import org.joda.time.DateTime;

import pl.edu.agh.pdwsm.automobil.ontology.Position;

public class PositionNotification extends AbstractJadeBehaviour {

    private long lastMessageTimestamp;
    private Position lastPosition = new Position(0, 0, DateTime.now(), 0.0);

    public PositionNotification(Agent a) {
        super(a);
    }

    @Override
    public void action() {
        if (System.currentTimeMillis() - lastMessageTimestamp > 5000) {
            Position position = getLastPosition();
            // logger.log(Level.INFO, "Sending current position: " + position);
            try {
                ACLMessage subscription = createMessage(ACLMessage.INFORM, "position");
                subscription.addReceiver(new AID(AbstractJadeBehaviour.AGENT_MANAGER, AID.ISLOCALNAME));
                myAgent.getContentManager().fillContent(subscription, position);
                myAgent.send(subscription);
                lastMessageTimestamp = System.currentTimeMillis();
            } catch (Codec.CodecException e) {
                e.printStackTrace();
            } catch (OntologyException e) {
                e.printStackTrace();
            }
        }
        block(1000);
    }

    public synchronized void updateLastKnownPosition(Position position) {
        lastPosition = position;
    }

    private synchronized Position getLastPosition() {
        return lastPosition;
    }

}