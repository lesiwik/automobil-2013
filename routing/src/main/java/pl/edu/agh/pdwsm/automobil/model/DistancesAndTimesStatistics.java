package pl.edu.agh.pdwsm.automobil.model;

public class DistancesAndTimesStatistics {
    double cummulativeDistance = 0.0;
    long cummulativeTime = 0;

    public void updateWith(double distance, int seconds) {
        cummulativeDistance += distance;
        cummulativeDistance += seconds;
    }

    public double getAverageSpeed() {
        return cummulativeDistance / cummulativeTime;
    }

    public void reset() {
        cummulativeDistance = 0;
        cummulativeTime = 0;
    }
}