package pl.edu.agh.pdwsm.automobil.agent;

import java.util.List;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;
import pl.edu.agh.pdwsm.automobil.ontology.Position;

public interface MobileAgent {

    void startRouting(Position startPoint, Position endPoint, boolean privileged);

    List<Edge> currentPath();

    void updatePosition(Position position);

    Position currentPosition();

    boolean isNavigating();
}
