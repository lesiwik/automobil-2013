package pl.edu.agh.pdwsm.automobil.traffic;

import pl.edu.agh.pdwsm.automobil.model.graph.Edge;

import java.io.Serializable;
import java.util.List;

public class TrafficQuery implements Serializable {
    private List<Edge> queryPath;

    public TrafficQuery(List<Edge> queryPath) {
        this.queryPath = queryPath;
    }

    public List<Edge> getQueryPath() {
        return queryPath;
    }
}
